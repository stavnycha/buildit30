package ee.ut.controller;
import static org.junit.Assert.assertTrue;

import java.util.GregorianCalendar;

import javax.ws.rs.core.MediaType;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import ee.ut.domain.RequestStatus;
import ee.ut.rest.PlantHireRequestResource;
import ee.ut.rest.PlantHireRequestResourceList;
import ee.ut.rest.PlantResourceList;
import ee.ut.rest.PurchaseOrderResource;
import ee.ut.rest.PurchaseOrderResourceList;
import ee.ut.rest.RejectBean;
import ee.ut.soap.client.InvalidHirePeriodException_Exception;
import ee.ut.soap.client.PlantSOAPService;
import ee.ut.soap.client.PlantSOAPServiceService;
import ee.ut.soap.client.PlantUnavailableException_Exception;
import ee.ut.soap.client.PurchaseOrderSOAPService;
import ee.ut.soap.client.PurchaseOrderSOAPServiceService;

public class RESTControllerTest {

    //private RESTController restController = new RESTController();
    private String server = "http://buildit30.herokuapp.com";//*/"http://localhost:8080/Buildit";

    private PlantSOAPService catalog = new PlantSOAPServiceService().getPlantSOAPServicePort();
    
    private PurchaseOrderSOAPService service = new PurchaseOrderSOAPServiceService().getPurchaseOrderSOAPServicePort();
	
    
    @Test
    public void getAllRequests() {
        org.junit.Assert.assertTrue(true);
    }

    @Test
    public void createHireRequest() {
    	Client client = Client.create(); 
    	WebResource webResource = client.resource(server + "/rest/requests"); 
    	PlantHireRequestResourceList initialList = webResource.type(MediaType.APPLICATION_XML) 
   			 .accept(MediaType.APPLICATION_XML).get(ClientResponse.class).getEntity(PlantHireRequestResourceList.class);
    	
    	PlantHireRequestResource resource = new PlantHireRequestResource();
    	resource.setPlant(1);
    	resource.setHireRequestId(1L);
    	//resource.setCost(400.);
    	resource.setStatus(RequestStatus.PENDING_CONFIRMATION);
    	resource.setSiteId(130);
    	resource.setStartDate(new GregorianCalendar(2013,4,4));
    	resource.setEndDate(new GregorianCalendar(2013,6,4));
    	 
    	ClientResponse response = webResource.type(MediaType.APPLICATION_XML) 
    	 .accept(MediaType.APPLICATION_XML).post(ClientResponse.class, resource); 
    	
    	System.out.println(response.getStatus());
    	assertTrue(response.getStatus() == ClientResponse.Status.CREATED.getStatusCode()); 
    	
    	PlantHireRequestResourceList eventualPOList = webResource.type(MediaType.APPLICATION_XML) 
   			 .accept(MediaType.APPLICATION_XML).get(ClientResponse.class).getEntity(PlantHireRequestResourceList.class);
    	assertTrue(initialList.getRequests().size() + 1 == eventualPOList.getRequests().size()); 
    }

    @Test
    public void modifyHireRequest() {
    	Client client = Client.create(); 
    	WebResource webResource = client.resource(server + "/rest/requests"); 
    	PlantHireRequestResourceList initialList = webResource.type(MediaType.APPLICATION_XML) 
   			 .accept(MediaType.APPLICATION_XML).get(ClientResponse.class).getEntity(PlantHireRequestResourceList.class);
    	
    	PlantHireRequestResource tomodify = initialList.getRequests().get(0);
    	
    	Long objId = tomodify.getHireRequestId();

    	ClientResponse response = webResource.type(MediaType.APPLICATION_XML) 
    	 .accept(MediaType.APPLICATION_XML).put(ClientResponse.class, tomodify); 
    	
    	assertTrue(response.getStatus() == ClientResponse.Status.OK.getStatusCode()); 

    }

    @Test
    public void cancelHireRequest() {
    	Client client = Client.create(); 
    	WebResource webResource = client.resource(server + "/rest/requests/"); 
    	PlantHireRequestResourceList initialList = webResource.type(MediaType.APPLICATION_XML) 
   			 .accept(MediaType.APPLICATION_XML).get(ClientResponse.class).getEntity(PlantHireRequestResourceList.class);
    	
    	PlantHireRequestResource toremove = initialList.getRequests().get(0);
    	webResource = client.resource(server + "/rest/requests/reject");
    	RejectBean reject = new RejectBean();
    	reject.setId(toremove.getHireRequestId());
    	reject.setComment("---");
    	ClientResponse response = webResource.type(MediaType.APPLICATION_XML)
    			.accept(MediaType.APPLICATION_XML).post(ClientResponse.class, reject) ; 
    	assertTrue(response.getStatus() == ClientResponse.Status.OK.getStatusCode()); 
    	
    	webResource = client.resource(server + "/rest/requests/" + toremove.getHireRequestId()); 
    	PlantHireRequestResource res = webResource.type(MediaType.APPLICATION_XML) 
   			 .accept(MediaType.APPLICATION_XML).get(ClientResponse.class).getEntity(PlantHireRequestResource.class);
       	assertTrue(res.getStatus() == RequestStatus.REJECTED); 
    }

    @Test 
    public void passPOtoRentit() throws JsonProcessingException{
    	
    	PlantHireRequestResource resource = new PlantHireRequestResource();
    	resource.setPlant(3);
    	resource.setHireRequestId(1L);
    	resource.setSiteId(140);
    	resource.setStartDate(new GregorianCalendar(2008,1,18));
    	resource.setEndDate(new GregorianCalendar(2008,1,20));
    	 
    	ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(resource);
    	HttpEntity<String> requestEntity = new HttpEntity<String>(json, Servers.getHeaders("rentit:rentit"));
		RestTemplate template = new RestTemplate();
		
		ResponseEntity<Void> response = template.exchange(server + "/rest/requests", 
				  HttpMethod.POST, requestEntity, Void.class);
		
		System.out.println("======" + response.getStatusCode().value());

    	assertTrue(response.getStatusCode().value() == ClientResponse.Status.CREATED.getStatusCode()); 
    	
    	System.out.println(response.getHeaders().getLocation());
    	ResponseEntity<PlantHireRequestResource> createdPHR = 
    			template.exchange(response.getHeaders().getLocation(), 
    					HttpMethod.GET, requestEntity, PlantHireRequestResource.class);
    	PlantHireRequestResource phr = createdPHR.getBody();
    			

    	requestEntity = new HttpEntity<String>(Servers.getHeaders("rentit:rentit"));
		
    	ResponseEntity<PurchaseOrderResource> po = template.exchange(phr.get_link("Accept Request").getHref().toString(), 
				  HttpMethod.POST, requestEntity, PurchaseOrderResource.class);
    	
    	ResponseEntity<PurchaseOrderResource> poRes = 
    			template.exchange(po.getBody().getId().getHref(), 
    					HttpMethod.GET, requestEntity, PurchaseOrderResource.class);
    	
    	assertTrue(poRes.getBody() != null);
    }
    
    @Test
    public void getRequest() {
        org.junit.Assert.assertTrue(true);
    }
    
    @Test
    public void testSoapPlants(){
    	Client client = Client.create(); 
    	WebResource webResource = client.resource("http://rentit30.herokuapp.com/rest/plants"); 
    	PlantResourceList listRest = webResource.type(MediaType.APPLICATION_XML) 
      	    	 .accept(MediaType.APPLICATION_XML).get(ClientResponse.class).getEntity(PlantResourceList.class);
    	ee.ut.soap.client.PlantResourceList listSoap = catalog.getAllPlants();
    	assertTrue(listRest.getPlants().size() == listSoap.getPlant().size());
    }
    @Test
    public void testSoapCreatePO() throws DatatypeConfigurationException, InvalidHirePeriodException_Exception, PlantUnavailableException_Exception{
    	Client client = Client.create(); 
    	WebResource webResource = client.resource("http://rentit30.herokuapp.com/rest/po"); 
    	PurchaseOrderResourceList initialList = webResource.type(MediaType.APPLICATION_XML) 
      	    	 .accept(MediaType.APPLICATION_XML).get(ClientResponse.class).getEntity(PurchaseOrderResourceList.class);
    	
    	GregorianCalendar start = new GregorianCalendar(2007,1,1), end = new GregorianCalendar(2007,2,2) ;

    	XMLGregorianCalendar startDay = DatatypeFactory.newInstance().newXMLGregorianCalendar(start);
		XMLGregorianCalendar endDay = DatatypeFactory.newInstance().newXMLGregorianCalendar(end);
		
    	ee.ut.soap.client.PurchaseOrderResource res = new ee.ut.soap.client.PurchaseOrderResource();
    	ee.ut.soap.client.PlantResource plant = new ee.ut.soap.client.PlantResource();
    	plant.setTitle("Truck");
    	
    	res.setEnd(endDay);
    	res.setEndDate(endDay);
    	res.setHireRequestId(1L);
    	res.setSiteId(444);
    	res.setStart(startDay);
    	res.setStartDate(startDay);
    	res.setPlant(plant);
    	res.setPaid(false);
    	service.createPO(res);
    	
    	PurchaseOrderResourceList finalList = webResource.type(MediaType.APPLICATION_XML) 
     	    	 .accept(MediaType.APPLICATION_XML).get(ClientResponse.class).getEntity(PurchaseOrderResourceList.class);
    	assertTrue(initialList.getOrders().size() + 1 == finalList.getOrders().size());
    	
    }
}
