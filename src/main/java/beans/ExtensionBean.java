package beans;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;

@RooJavaBean
public class ExtensionBean {
	private Long id;
	
	@Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
	private Date endDate;
}
