package beans;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;

@XmlRootElement
@RooJavaBean
public class POModification {
	private Long id;
	
	@Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
	private Date startDate;
	
	@Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
	private Date endDate;
}
