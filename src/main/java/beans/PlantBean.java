package beans;

import org.springframework.roo.addon.javabean.RooJavaBean;

@RooJavaBean
public class PlantBean {

	private String title;
	private Integer plantId;
	private Double price;
	private String description;
	private String serverPlant;
	
	@Override
    public String toString(){
      	return title + " " + description + " " + price;
    }
}
