package ee.ut.controller.rentits;

public class RentitControllerFabric {

	
	static public RentitController getController(String server){
		
		RentitController res = null;
		
		switch (server) {
			case RentitServers.RENTIT_30: 
				res = new Rentit30Controller();
				break;
			case RentitServers.RENTIT_1: 
				res = new Rentit1Controller();
				break;
			case RentitServers.RENTIT_9: 
				res = new Rentit9Controller();
				break;
			default: break;
		}
		
		return res;
	}
}
