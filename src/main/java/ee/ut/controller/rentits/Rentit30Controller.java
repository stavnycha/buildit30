package ee.ut.controller.rentits;

import java.io.IOException;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;

import beans.ExtensionBean;
import beans.POModification;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import ee.ut.controller.Servers;
import ee.ut.domain.Invoice;
import ee.ut.domain.Planthirerequest;
import ee.ut.domain.RequestStatus;
import ee.ut.rest.InvoiceResource;
import ee.ut.rest.PlantResource;
import ee.ut.rest.PlantResourceList;
import ee.ut.rest.PurchaseOrderAssembler;
import ee.ut.rest.PurchaseOrderResource;
import ee.ut.rest.QueriedPlant;
import ee.ut.service.PlantHireRequestService;

public class Rentit30Controller implements RentitController {

	private final String email = RentitServers.RENTIT_30_EMAIL;
	private final String server = RentitServers.RENTIT_30;// "http://rentit30.herokuapp.com";
	private final String credentials = "buildit:buildit";

	@Autowired
	private PlantHireRequestService requestService;

	public String getEmail() {
		return email;
	}

	public String getServer() {
		return server;
	}

	public String getCredentials() {
		return credentials;
	}

	public PurchaseOrderResource createPO(PurchaseOrderResource po)
			throws JsonProcessingException {
		ObjectWriter ow = new ObjectMapper().writer()
				.withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(po);
		HttpEntity<String> requestEntity = new HttpEntity<String>(json,
				Servers.getHeaders(credentials));

		RestTemplate template = new RestTemplate();
		template.setErrorHandler(new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse response)
					throws IOException {
				return false;
			}

			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {
				return;
			}
		});

		try {
			ResponseEntity<PurchaseOrderResource> response = template.exchange(
					server + "/rest/po", HttpMethod.POST, requestEntity,
					PurchaseOrderResource.class);
			return response.getBody();
		} catch (Exception e) {
			Planthirerequest phr = Planthirerequest.findPlanthirerequest(po.getHireRequestId());
			phr.setStatus(RequestStatus.REJECTED);
			phr.persist();
			e.printStackTrace();
			return null;
		}

	}

	public PurchaseOrderResource getPO(URI location) {
		HttpEntity<String> requestEntity = new HttpEntity<String>(
				Servers.getHeaders(credentials));
		RestTemplate template = new RestTemplate();
		ResponseEntity<PurchaseOrderResource> res = template.exchange(location,
				HttpMethod.GET, requestEntity, PurchaseOrderResource.class);
		return res.getBody();
	}

	public PurchaseOrderResource getPO(Long id) {
		HttpEntity<String> requestEntity = new HttpEntity<String>(
				Servers.getHeaders(credentials));
		RestTemplate template = new RestTemplate();
		ResponseEntity<PurchaseOrderResource> res = template.exchange(server
				+ "/rest/po/" + id, HttpMethod.GET, requestEntity,
				PurchaseOrderResource.class);
		return res.getBody();
	}

	public PlantResource getPlant(Integer id) {
		HttpEntity<String> requestEntity = new HttpEntity<String>(
				Servers.getHeaders(credentials));
		RestTemplate template = new RestTemplate();
		ResponseEntity<PlantResource> response = template.exchange(server
				+ "/rest/plants/" + id, HttpMethod.GET, requestEntity,
				PlantResource.class);
		return response.getBody();
	}

	public void acceptInvoice(Invoice invoice) {
		HttpEntity<String> requestEntity = new HttpEntity<String>(
				Servers.getHeaders(credentials));
		RestTemplate template = new RestTemplate();

		ResponseEntity<Object> res = template.exchange(server
				+ "/rest/po/acceptInvoice/" + invoice.getInvoiceNumber(), HttpMethod.GET,
				requestEntity, Object.class);
	}

	public PlantResourceList getAvailablePlants(QueriedPlant plant)
			throws JsonProcessingException {
		ObjectWriter ow = new ObjectMapper().writer()
				.withDefaultPrettyPrinter();
		// String json = ow.writeValueAsString(plant);
		HttpEntity<String> requestEntity = new HttpEntity<String>(
				Servers.getHeaders(credentials));
		RestTemplate template = new RestTemplate();
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		ResponseEntity<PlantResourceList> response = template.exchange(server
				+ "/rest/plants/available?name=" + plant.getName() + "&start="
				+ format.format(plant.getStartDate().getTime()) + "&end="
				+ format.format(plant.getEndDate().getTime()), HttpMethod.GET,
				requestEntity, PlantResourceList.class);
		return response.getBody();
	}

	public boolean cancelPO(Long id) {
		HttpEntity<String> requestEntity = new HttpEntity<String>(
				Servers.getHeaders(credentials));
		RestTemplate template = new RestTemplate();
		template.setErrorHandler(new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse response)
					throws IOException {
				return false;
			}

			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {
				return;
			}
		});

		Planthirerequest phr = Planthirerequest.findPlanthirerequest(id);
		ResponseEntity<PurchaseOrderResource> res = template.exchange(server
				+ "/rest/po/cancel/" + phr.getPoid(), HttpMethod.GET,
				requestEntity, PurchaseOrderResource.class);
		if (res.getStatusCode() == HttpStatus.OK) {
			phr.setStatus(RequestStatus.CLOSED);
			phr.persist();
			return true;
		} else {
			return false;
		}
	}

	public void requestUpdate(ExtensionBean bean)
			throws JsonProcessingException {

		Planthirerequest phr = Planthirerequest.findPlanthirerequest(bean
				.getId());
		PurchaseOrderAssembler assembler = new PurchaseOrderAssembler();
		PurchaseOrderResource resource = assembler.toResource(phr);
		resource.setEnd(bean.getEndDate());

		ObjectWriter ow = new ObjectMapper().writer()
				.withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(resource);
		HttpEntity<String> requestEntity = new HttpEntity<String>(json,
				Servers.getHeaders(credentials));
		RestTemplate template = new RestTemplate();
		template.setErrorHandler(new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse response)
					throws IOException {
				return false;
			}

			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {
				return;
			}
		});

		ResponseEntity<PurchaseOrderResource> res = template.exchange(server
				+ "/rest/po/update", HttpMethod.POST, requestEntity,
				PurchaseOrderResource.class);
		if (res.getStatusCode() == HttpStatus.OK) {
			Calendar date = new GregorianCalendar();
			date.setTime(bean.getEndDate());
			phr.setEndDate(date);
			phr.setComment("Extended");
			phr.setPrice(res.getBody().getPrice());
			phr.persist();
		} else {
			phr.setComment("Extension is rejected");
			phr.persist();
		}
	}

	@Override
	public Double getPriceFromInvoice(Document invoice)
			throws XPathExpressionException, JAXBException {
		XPath xPath = XPathFactory.newInstance().newXPath();
		return Double.parseDouble(xPath.evaluate("//total", invoice));
	}

	@Override
	public String getRemittanceAdvice(InvoiceResource i) { 
		return "Invoice number: " + i.getInvoiceId() + ". The sum of " + i.getTotal()
				+ " has been paid.";
	}

	@Override
	public InvoiceResource getInvoice(Document invoiceXML) throws JAXBException, ParseException {

		JAXBContext jaxbCtx = JAXBContext.newInstance(InvoiceResource.class);
		InvoiceResource invoice = (InvoiceResource) jaxbCtx
				.createUnmarshaller().unmarshal(invoiceXML);
		return invoice;

	}

	@Override
	public PurchaseOrderResource modifyPO(POModification m) throws JsonProcessingException {
		Planthirerequest phr = Planthirerequest.findPlanthirerequest(m.getId());
		PurchaseOrderAssembler a = new PurchaseOrderAssembler();
		
		Calendar start = phr.getStartDate(), end = phr.getEndDate(), 
				c1 = Calendar.getInstance(), c2 = Calendar.getInstance();
		c1.setTime(m.getStartDate());
		c2.setTime(m.getEndDate());
		phr.setStartDate(c1);
		phr.setEndDate(c2);
		
		ObjectWriter ow = new ObjectMapper().writer()
				.withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(a.toResource(phr));
		HttpEntity<String> requestEntity = new HttpEntity<String>(json,
				Servers.getHeaders(credentials));
		RestTemplate template = new RestTemplate();
		
		template.setErrorHandler(new ResponseErrorHandler() {
			@Override
			public boolean hasError(ClientHttpResponse response)
					throws IOException {
				return false;
			}

			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {
				return;
			}
		});
		
		try{
			ResponseEntity<PurchaseOrderResource> response = template.exchange(server
				+ "/rest/po/modify", HttpMethod.POST,
				requestEntity, PurchaseOrderResource.class);
			phr.setComment("Modified.");
			phr.setPrice(response.getBody().getPrice());
			phr.persist();
			return a.toResource(phr);
			
		} catch (Exception e){
			e.printStackTrace();
			phr.setEndDate(end);
			phr.setStartDate(start);
			phr.setComment("Modification is rejected");
			
			phr.persist();
			return null;
		}
	}

	@Override
	public PlantResourceList getAllPlants() {
		HttpEntity<String> requestEntity = new HttpEntity<String>(
				Servers.getHeaders(credentials));
		RestTemplate template = new RestTemplate();
		ResponseEntity<PlantResourceList> response = template.exchange(server
				+ "/rest/plants", HttpMethod.GET, requestEntity,
				PlantResourceList.class);
		return response.getBody();
	}

}
