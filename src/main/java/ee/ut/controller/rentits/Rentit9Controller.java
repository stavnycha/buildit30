package ee.ut.controller.rentits;

import java.io.IOException;
import java.net.URI;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import ee.ut.controller.Servers;
import org.joda.time.DateMidnight;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;

import beans.ExtensionBean;
import beans.POModification;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

import ee.ut.domain.Invoice;
import ee.ut.domain.Planthirerequest;
import ee.ut.domain.RequestStatus;
import ee.ut.rest.InvoiceResource;
import ee.ut.rest.PlantResource;
import ee.ut.rest.PlantResourceList;
import ee.ut.rest.PurchaseOrderAssembler;
import ee.ut.rest.PurchaseOrderResource;
import ee.ut.rest.QueriedPlant;
import ee.ut.rest.rentit9.PlantResourceConverter;
import ee.ut.rest.rentit9.PurchaseOrderResourceConverter;
import ee.ut.rest.rentit9.RemittanceAdviceResource;

public class Rentit9Controller implements RentitController {

    private final String email = RentitServers.RENTIT_9_EMAIL;
    private final String server = RentitServers.RENTIT_9;// "http://rentit30.herokuapp.com";
    private final String credentials = "buildit30:buildit30";

    @Override
    public PurchaseOrderResource createPO(PurchaseOrderResource po)
            throws JsonProcessingException {
        PurchaseOrderResourceConverter conv = new PurchaseOrderResourceConverter();
        ee.ut.rest.rentit9.PurchaseOrderResource res = conv.fromResource(po);
        ClientResponse createRequest = getClient().resource(server + "/rest/pos")
                .type(MediaType.APPLICATION_XML)
                .post(ClientResponse.class, res);

        int status = createRequest.getStatus();
        if (status != ClientResponse.Status.CREATED.getStatusCode()) {
            Planthirerequest phr = Planthirerequest.findPlanthirerequest(po.getHireRequestId());
            phr.setStatus(RequestStatus.REJECTED);
            phr.persist();
            return null;
        } else {
            return conv.toResource(createRequest.getEntity(ee.ut.rest.rentit9.PurchaseOrderResource.class));
        }
    }

    @Override
    public PurchaseOrderResource getPO(URI location) {
        WebResource webResource = getClient().resource(location);
        ClientResponse request = webResource.get(ClientResponse.class);

        PurchaseOrderResourceConverter conv = new PurchaseOrderResourceConverter();
        return conv.toResource(request.getEntity(ee.ut.rest.rentit9.PurchaseOrderResource.class));
    }

    @Override
    public PurchaseOrderResource getPO(Long id) {
        WebResource webResource = getClient().resource(server + "/rest/pos/" + id);
        ClientResponse request = webResource.get(ClientResponse.class);

        PurchaseOrderResourceConverter conv = new PurchaseOrderResourceConverter();
        return conv.toResource(request.getEntity(ee.ut.rest.rentit9.PurchaseOrderResource.class));
    }

    @Override
    public PlantResource getPlant(Integer id) {
        WebResource webResource = getClient().resource(server + "/rest/plants/" + id);
        ClientResponse request = webResource.get(ClientResponse.class);

        PlantResourceConverter conv = new PlantResourceConverter();
        return conv.toResource(request.getEntity(ee.ut.rest.rentit9.PlantResource.class));
    }

    @Override
    public PlantResourceList getAvailablePlants(QueriedPlant plant)
            throws JsonProcessingException {
        WebResource webResource = getClient().resource(getFindUrl(plant.getName(),
                new DateMidnight(plant.getStartDate()), new DateMidnight(plant.getEndDate())));
        ClientResponse request = webResource.get(ClientResponse.class);

        PlantResourceConverter conv = new PlantResourceConverter();
        return conv.toResource(request.getEntity(ee.ut.rest.rentit9.PlantResourceList.class));
    }

    @Override
    public boolean cancelPO(Long id) {
        Planthirerequest phr = Planthirerequest.findPlanthirerequest(id);
        WebResource webResource = getClient().resource(server + "/rest/pos/" + phr.getPoid());
        ClientResponse request = webResource.delete(ClientResponse.class);
        if (request.getStatus() == ClientResponse.Status.OK.getStatusCode()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void requestUpdate(ExtensionBean bean)
            throws JsonProcessingException {
        Planthirerequest phr = Planthirerequest.findPlanthirerequest(bean.getId());
        PurchaseOrderAssembler assembler = new PurchaseOrderAssembler();
        PurchaseOrderResourceConverter conv = new PurchaseOrderResourceConverter();

        Calendar c = Calendar.getInstance();
        c.setTime(bean.getEndDate());
        phr.setEndDate(c);
        Calendar end = phr.getEndDate();
        ClientResponse createRequest = getClient().resource(server + "/rest/pos/" + phr.getPoid())
                .type(MediaType.APPLICATION_XML)
                .put(ClientResponse.class, conv.fromResource(assembler.toResource(phr)));

        int status = createRequest.getStatus();
        if (status != ClientResponse.Status.OK.getStatusCode()) {
            phr.setEndDate(end);
            phr.setComment("Extension is rejected.");
            return;
        } else {
            phr.setComment("Extended.");
            phr.persist();
            return;
        }
    }

    @Override
    public void acceptInvoice(Invoice invoice) {
        // TODO Auto-generated method stub
        //Invoice invoice = Invoice.findInvoice(id);
        RemittanceAdviceResource ra = new RemittanceAdviceResource();
        ra.setRentitInvoiceId(invoice.getInvoiceNumber());
        Calendar c = Calendar.getInstance();
        c.setTime(invoice.getDate());
        ra.setPaymentDate(c);

        ClientResponse createRequest = getClient().resource(server + "/rest/ra")
                .type(MediaType.APPLICATION_XML)
                .post(ClientResponse.class, ra);

        int status = createRequest.getStatus();
        if (status != ClientResponse.Status.CREATED.getStatusCode()) {
            //TODO sth?
            return;
        } else {
            return;
        }

    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getServer() {
        return server;
    }

    @Override
    public String getCredentials() {
        return credentials;
    }

    @Override
    public Double getPriceFromInvoice(Document invoice)
            throws XPathExpressionException, JAXBException {
        XPath xPath = XPathFactory.newInstance().newXPath();
        return Double.parseDouble(xPath.evaluate("//total", invoice));
    }

    @Override
    public String getRemittanceAdvice(InvoiceResource i) {
        return "Invoice number: " + i.getInvoiceId() + ". The sum of " + i.getTotal()
                + " has been paid.";
    }

    @Override
    public InvoiceResource getInvoice(Document invoiceXML)
            throws JAXBException, ParseException {
        JAXBContext jaxbCtx = JAXBContext.newInstance(ee.ut.rest.rentit9.InvoiceResource.class);

        ee.ut.rest.rentit9.InvoiceResource invoice = (ee.ut.rest.rentit9.InvoiceResource) jaxbCtx
                .createUnmarshaller().unmarshal(invoiceXML);

        InvoiceResource res = new InvoiceResource();
        res.setDate(invoice.getDate());
        res.setInvoiceId(invoice.getInvoiceId());
        res.setPoId(invoice.getPoId());
        res.setRequestId(invoice.getRequestId());
        res.setTotal(invoice.getTotal());
        res.setStatus(ee.ut.domain.InvoiceStatus.NOT_PAID);
        return res;
    }

    private static Client getClient() {
        Client client = Client.create();
        client.addFilter(new HTTPBasicAuthFilter("buildit30", "buildit30"));
        return client;
    }

    private String getFindUrl(String name, DateMidnight startDate, DateMidnight endDate) {
        DateTimeFormatter fmt = ISODateTimeFormat.yearMonthDay();
        return String.format("%s/find?name=%s&start=%s&end=%s",
                server + "/rest/plants", name, startDate.toString(fmt), endDate.toString(fmt));
    }

    @Override
    public PurchaseOrderResource modifyPO(POModification m)  {
        Planthirerequest phr = Planthirerequest.findPlanthirerequest(m.getId());
        PurchaseOrderAssembler a = new PurchaseOrderAssembler();
        PurchaseOrderResourceConverter conv = new PurchaseOrderResourceConverter();


        Calendar start = phr.getStartDate(),
                end = phr.getEndDate(),
                c1 = Calendar.getInstance(),
                c2 = Calendar.getInstance();
        c1.setTime(m.getStartDate());
        c2.setTime(m.getEndDate());
        phr.setStartDate(c1);
        phr.setEndDate(c2);

        ClientResponse modifyRequest = getClient().resource(server + "/rest/pos/" + phr.getPoid())
                .type(MediaType.APPLICATION_XML)
                .put(ClientResponse.class, conv.fromResource(a.toResource(phr)));

        int status = modifyRequest.getStatus();
        if (status == ClientResponse.Status.OK.getStatusCode()) {
            phr.merge();
            return conv.toResource(modifyRequest.getEntity(ee.ut.rest.rentit9.PurchaseOrderResource.class));
        }

        return null;
    }

    @Override
    public PlantResourceList getAllPlants() {
        WebResource webResource = getClient().resource(server + "/rest/plants");
        ClientResponse request = webResource.get(ClientResponse.class);

        PlantResourceConverter conv = new PlantResourceConverter();
        return conv.toResource(request.getEntity(ee.ut.rest.rentit9.PlantResourceList.class));
    }

}
