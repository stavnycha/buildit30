package ee.ut.controller.rentits;

import java.io.IOException;
import java.net.URI;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import ee.ut.rest.*;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;

import beans.ExtensionBean;
import beans.POModification;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import ee.ut.controller.RentitAPIController;
import ee.ut.controller.Servers;
import ee.ut.domain.Invoice;
import ee.ut.domain.Planthirerequest;
import ee.ut.domain.RequestStatus;
import ee.ut.rest.rentit1.InvoiceResourceConverter;
import ee.ut.rest.rentit1.POExtensionResource;
import ee.ut.rest.rentit1.PlantResourceConverter;
import ee.ut.rest.rentit1.PurchaseOrderResource1Convertor;
import ee.ut.rest.rentit1.PurchaseOrderResourceConvertor;
import ee.ut.rest.rentit1.InvoiceResource;

public class Rentit1Controller implements RentitController {

    private final String email = RentitServers.RENTIT_1_EMAIL;
    private final String server = RentitServers.RENTIT_1;
    private final String credentials = "customer:customer";

    @Override
    public PurchaseOrderResource createPO(PurchaseOrderResource po)
            throws JsonProcessingException {

        PurchaseOrderResourceConvertor conv = new PurchaseOrderResourceConvertor();
        PurchaseOrderResource1Convertor conv1 = new PurchaseOrderResource1Convertor();


        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(conv1.toResource(po));
        HttpEntity<String> requestEntity = new HttpEntity<String>(json, Servers.getHeaders(credentials));

        RestTemplate template = new RestTemplate();
        template.setErrorHandler(new ResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                return false;
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException {
                return;
            }
        });

        try {
            ResponseEntity<ee.ut.rest.rentit1.PurchaseOrderResource> response = template.exchange(server + "/rest/pos",
                    HttpMethod.POST, requestEntity, ee.ut.rest.rentit1.PurchaseOrderResource.class);
            return conv.toResource(response.getBody());
        } catch (Exception e) {
            Planthirerequest phr = Planthirerequest.findPlanthirerequest(po.getHireRequestId());
            phr.setStatus(RequestStatus.REJECTED);
            phr.persist();
            e.printStackTrace(); // you are not suppose to print stack trace. change it.
            return null;
        }

    }

    @Override
    public PurchaseOrderResource getPO(URI location) {
        HttpEntity<String> requestEntity = new HttpEntity<String>(Servers.getHeaders(credentials));
        RestTemplate template = new RestTemplate();
        ResponseEntity<ee.ut.rest.rentit1.PurchaseOrderResource> res =
                template.exchange(location, HttpMethod.GET, requestEntity, ee.ut.rest.rentit1.PurchaseOrderResource.class);
        PurchaseOrderResourceConvertor conv = new PurchaseOrderResourceConvertor();
        return conv.toResource(res.getBody());
    }

    @Override
    public PurchaseOrderResource getPO(Long id) {
        HttpEntity<String> requestEntity = new HttpEntity<String>(Servers.getHeaders(credentials));
        RestTemplate template = new RestTemplate();
        ResponseEntity<ee.ut.rest.rentit1.PurchaseOrderResource> res =
                template.exchange(server + "/rest/pos/" + id, HttpMethod.GET, requestEntity, ee.ut.rest.rentit1.PurchaseOrderResource.class);
        PurchaseOrderResourceConvertor conv = new PurchaseOrderResourceConvertor();
        return conv.toResource(res.getBody());
    }

    @Override
    public PlantResource getPlant(Integer id) {
        HttpEntity<String> requestEntity = new HttpEntity<String>(Servers.getHeaders(credentials));
        RestTemplate template = new RestTemplate();
        ResponseEntity<ee.ut.rest.rentit1.PlantResource> res =
                template.exchange(server + "/rest/plants/" + id, HttpMethod.GET, requestEntity, ee.ut.rest.rentit1.PlantResource.class);

        PlantResourceConverter conv = new PlantResourceConverter();
        return conv.toResource(res.getBody());
    }

    @Override
    public void acceptInvoice(Invoice invoice) {
        // TODO Auto-generated method stub

    }

    @Override
    public PlantResourceList getAvailablePlants(QueriedPlant plant)
            throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(plant);
        HttpEntity<String> requestEntity = new HttpEntity<String>(json, Servers.getHeaders(credentials));
        RestTemplate template = new RestTemplate();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        ResponseEntity<ee.ut.rest.rentit1.PlantResourceList> response =
                template.exchange(server + "/rest/plants?plantName=" + plant.getName() + "&startDate=" +
                        df.format(plant.getStartDate().getTime()) + "&endDate=" + df.format(plant.getEndDate().getTime()),
                        HttpMethod.GET, requestEntity, ee.ut.rest.rentit1.PlantResourceList.class);
        ee.ut.rest.rentit1.PlantResourceList res = response.getBody();
        PlantResourceConverter conv = new PlantResourceConverter();
        return conv.toResource(res);
    }

    @Override
    public boolean cancelPO(Long id) {
        HttpEntity<String> requestEntity = new HttpEntity<String>(Servers.getHeaders(credentials));
        RestTemplate template = new RestTemplate();
        template.setErrorHandler(new ResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                return false;
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException {
                return;
            }
        });

        Planthirerequest phr = Planthirerequest.findPlanthirerequest(id);
        ResponseEntity<ee.ut.rest.rentit1.PurchaseOrderResource> res = template.exchange(server + "/rest/pos/" + phr.getPoid() + "/cancel",
                HttpMethod.DELETE, requestEntity, ee.ut.rest.rentit1.PurchaseOrderResource.class);
        if (res.getStatusCode() == HttpStatus.OK) {
            phr.setStatus(RequestStatus.CLOSED);
            phr.persist();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void requestUpdate(ExtensionBean bean)
            throws JsonProcessingException {
        Planthirerequest phr = Planthirerequest.findPlanthirerequest(bean.getId());
        POExtensionResource ext = new POExtensionResource();
        ext.setEnddate(bean.getEndDate());
        ext.setStartdate(phr.getStartDate().getTime());

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(ext);
        HttpEntity<String> requestEntity = new HttpEntity<String>(json, Servers.getHeaders(credentials));
        RestTemplate template = new RestTemplate();
        template.setErrorHandler(new ResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                if (HttpStatus.BAD_REQUEST.equals(response.getStatusCode())) {
                    return true;
                }
                return false;
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException {
                response.getRawStatusCode();
                return;
            }
        });
        try {
            ResponseEntity<POExtensionResource> res = template.exchange(server + "/rest/pos/" + phr.getPoid() + "/updates",
                    HttpMethod.POST, requestEntity, POExtensionResource.class);
            Calendar c = Calendar.getInstance();
            c.setTime(bean.getEndDate());
            phr.setEndDate(c);

            DateTime startDate = new DateTime(phr.getStartDate());
            DateTime endDate = new DateTime(phr.getEndDate());
            int days = Days.daysBetween(startDate, endDate).getDays();

            PlantResource plant = RentitAPIController.getPlant(phr);
            phr.setPrice(days * plant.getPrice());
            phr.setComment("Extension requested. Check PO status on " + server);
            phr.persist();
        } catch (Exception e) {
            phr.setComment("Extension is rejected");
            phr.persist();
        }

    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getServer() {
        return server;
    }

    @Override
    public String getCredentials() {
        return credentials;
    }

    @Override
    public Double getPriceFromInvoice(Document invoice) throws XPathExpressionException, JAXBException {
        XPath xPath = XPathFactory.newInstance().newXPath();
        return Double.parseDouble(xPath.evaluate("//total", invoice));
    }

    @Override
    public String getRemittanceAdvice(ee.ut.rest.InvoiceResource i) {
        return "Remittance Advice for Purchase Order Id:-" + i.getPoId() + "-\n" +
                "Invoice Id:-" + i.getInvoiceId() + "-";
        /*SimpleMailMessage mailMessage = new SimpleMailMessage();

		mailMessage.setTo(email);
		mailMessage.setSentDate(new Date());
		mailMessage.setSubject("Remmitance Advice");
		
		mailMessage.setText("Remittance Advice for Purchase Order Id:-" + invoice.getPoId() + "-\n" + 
				"Invoice Id:-" + invoice.getInvoiceId() + "-");

		mailSender.send(mailMessage);*/
    }

    @Override
    public ee.ut.rest.InvoiceResource getInvoice(Document invoiceXML) throws JAXBException, ParseException {
        JAXBContext jaxbCtx = JAXBContext
                .newInstance(InvoiceResource.class);
        InvoiceResource invoice = (InvoiceResource) jaxbCtx
                .createUnmarshaller().unmarshal(invoiceXML);
        InvoiceResourceConverter converter = new InvoiceResourceConverter();
        return converter.toResource(invoice);
    }

    @Override
    public PurchaseOrderResource modifyPO(POModification m) throws JsonProcessingException {
        Planthirerequest phr = Planthirerequest.findPlanthirerequest(m.getId());
        PurchaseOrderAssembler a = new PurchaseOrderAssembler();

        Calendar start = phr.getStartDate(), end = phr.getEndDate(),
                c1 = Calendar.getInstance(), c2 = Calendar.getInstance();
        c1.setTime(m.getStartDate());
        c2.setTime(m.getEndDate());
        phr.setStartDate(c1);
        phr.setEndDate(c2);

        PurchaseOrderResourceConvertor conv = new PurchaseOrderResourceConvertor();
        PurchaseOrderResource1Convertor conv1 = new PurchaseOrderResource1Convertor();


        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(conv1.toResource(a.toResource(phr)));
        HttpEntity<String> requestEntity = new HttpEntity<String>(json, Servers.getHeaders(credentials));

        RestTemplate template = new RestTemplate();
        template.setErrorHandler(new ResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                if (HttpStatus.BAD_REQUEST.equals(response.getStatusCode())) {
                    return true;
                } else if (HttpStatus.METHOD_NOT_ALLOWED.equals(response.getStatusCode())) {
                    return true;
                } else {
                    return false;
                }
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException {
                if (HttpStatus.BAD_REQUEST.equals(response.getStatusCode())) {

                } else if (HttpStatus.METHOD_NOT_ALLOWED.equals(response.getStatusCode())) {

                } else if (HttpStatus.NOT_FOUND.equals(response.getStatusCode())) {

                }
            }
        });

        try {
            ResponseEntity<ee.ut.rest.rentit1.PurchaseOrderResource> response = template.exchange(server + "/rest/pos",
                    HttpMethod.POST, requestEntity, ee.ut.rest.rentit1.PurchaseOrderResource.class);
            phr.persist();
            return conv.toResource(response.getBody());
        } catch (Exception e) {
            e.printStackTrace(); // you are not suppose to print stack trace. change it.

        }
        return null;

    }

    @Override
    public PlantResourceList getAllPlants() {
        HttpEntity<String> requestEntity = new HttpEntity<String>(Servers.getHeaders(credentials));
        RestTemplate template = new RestTemplate();
        ResponseEntity<ee.ut.rest.rentit1.PlantResourceList> res =
                template.exchange(server + "/rest/plants", HttpMethod.GET, requestEntity, ee.ut.rest.rentit1.PlantResourceList.class);

        PlantResourceConverter conv = new PlantResourceConverter();
        return conv.toResource(res.getBody());
    }

}
