package ee.ut.controller.rentits;

import java.util.ArrayList;
import java.util.List;

public class RentitServers {

	public final static String RENTIT_30 = //"http://localhost:8080/Rentit";//*/
			"http://rentit30.herokuapp.com";
	public final static String RENTIT_1 = "http://rentit1.herokuapp.com";
	public final static String RENTIT_1_EMAIL = "esi.rent1@gmail.com";
	public final static String RENTIT_30_EMAIL = "esi.rentit@gmail.com";
	public final static String RENTIT_9 = "http://rentit9.herokuapp.com";
	public final static String RENTIT_9_EMAIL = "rentit9esi@gmail.com";
	
	public List<String> getAllServers(){
		List<String> res = new ArrayList<>();
		res.add(RENTIT_1);
		res.add(RENTIT_30);
		res.add(RENTIT_9);
		return res;
	}
	
	static public String getServerFromEmail(String email){
		String res = null;
		switch (email){
			case RENTIT_30_EMAIL: res = RENTIT_30; break;
			case RENTIT_1_EMAIL: res = RENTIT_1; break;
			case RENTIT_9_EMAIL: res = RENTIT_9; break;
			default:break;
		}
		return res;
	}
	
	static public String getEmailFromServer(String server){
		String res = null;
		switch (server){
			case RENTIT_30: res = RENTIT_30_EMAIL; break;
			case RENTIT_1: res = RENTIT_1_EMAIL; break;
			case RENTIT_9_EMAIL: res = RENTIT_9; break;
			default:break;
		}
		return res;
	}
}
