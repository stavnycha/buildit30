package ee.ut.controller.rentits;

import java.net.URI;
import java.text.ParseException;

import javax.xml.bind.JAXBException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;

import beans.ExtensionBean;
import beans.POModification;

import com.fasterxml.jackson.core.JsonProcessingException;

import ee.ut.domain.Invoice;
import ee.ut.rest.InvoiceResource;
import ee.ut.rest.PlantResource;
import ee.ut.rest.PlantResourceList;
import ee.ut.rest.PurchaseOrderResource;
import ee.ut.rest.QueriedPlant;

public interface RentitController {

	public PurchaseOrderResource createPO(PurchaseOrderResource po) throws JsonProcessingException;
	
	public PurchaseOrderResource getPO(URI location);
	
	public PurchaseOrderResource getPO(Long id);
	
	public PlantResource getPlant(Integer id);
	
	public void acceptInvoice(Invoice invoice);
	
	public PlantResourceList getAvailablePlants(QueriedPlant plant) throws JsonProcessingException;
	
	public boolean cancelPO(Long id);
	
	public void requestUpdate(ExtensionBean bean) throws JsonProcessingException;

	public String getEmail();

	public String getServer();

	public String getCredentials();

	public Double getPriceFromInvoice(Document invoice) throws XPathExpressionException, JAXBException;

	public String getRemittanceAdvice(InvoiceResource i);

	public InvoiceResource getInvoice(Document invoiceXML) throws JAXBException, ParseException;

	public PurchaseOrderResource modifyPO(POModification m) throws JsonProcessingException ;

	public PlantResourceList getAllPlants();
}