package ee.ut.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import ee.ut.controller.rentits.RentitServers;
import ee.ut.domain.Planthirerequest;
import ee.ut.domain.RequestStatus;
import ee.ut.exceptions.EngineerNotFoundException;
import ee.ut.exceptions.InvalidHirePeriodException;
import ee.ut.exceptions.MethodNotAllowedException;
import ee.ut.exceptions.RequestNotFoundException;
import ee.ut.rest.PlantHireRequestResource;
import ee.ut.rest.PlantHireRequestResourceAssembler;
import ee.ut.rest.PlantHireRequestResourceList;
import ee.ut.rest.PlantResource;
import ee.ut.rest.PurchaseOrderAssembler;
import ee.ut.rest.PurchaseOrderResource;
import ee.ut.rest.RejectBean;
import ee.ut.rest.rentit1.RejectPOResource;
import ee.ut.service.PlantHireRequestService;
import ee.ut.utils.ExtendedLink;


@Controller
@RequestMapping("/rest/requests")
public class RESTController {
	
	@Autowired 
	private PlantHireRequestService requestService;

	////// for rentit1
	@RequestMapping(method = RequestMethod.POST, value = "/rejectPHR")
	public ResponseEntity<Void> rejectPHR(
			@RequestBody RejectPOResource rpor) throws RequestNotFoundException, MethodNotAllowedException {
		RejectBean bean = new RejectBean();
		bean.setComment(rpor.getCommentt());
		Planthirerequest phr = Planthirerequest.findPlanthirerequestByPOId(rpor.getPoid(), RentitServers.RENTIT_1);
		bean.setId(phr.getId());
		return rejectHireRequest(bean);
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/rejectPHRExtension")
	public ResponseEntity<Void> rejectPHRExtension(
			@RequestBody RejectPOResource rpor) throws RequestNotFoundException, MethodNotAllowedException {
		RejectBean bean = new RejectBean();
		bean.setComment(rpor.getCommentt());
		Planthirerequest phr = Planthirerequest.findPlanthirerequestByPOId(rpor.getPoid(), RentitServers.RENTIT_1);
		bean.setId(phr.getId());
		PurchaseOrderResource po = RentitAPIController.getPO(phr);
		Calendar c = Calendar.getInstance();
		c.setTime(po.getEnd());
		
		DateTime startDate = new DateTime(phr.getStartDate());
		DateTime endDate = new DateTime(phr.getEndDate());
		int days = Days.daysBetween(startDate, endDate).getDays();

		PlantResource plant = RentitAPIController.getPlant(phr);
		phr.setPrice(days * plant.getPrice());
		
		phr.setEndDate(c);
		phr.persist();
		return rejectHireRequest(bean);
	}
	///////
	
	
	@RequestMapping(method = RequestMethod.GET, value = "")
	public ResponseEntity<PlantHireRequestResourceList> getAllRequests() {
		List<Planthirerequest> pos = Planthirerequest.findAllPlanthirerequests();

		PlantHireRequestResourceAssembler assembler = new PlantHireRequestResourceAssembler();
		PlantHireRequestResourceList resList = assembler.toResource(pos);

		ResponseEntity<PlantHireRequestResourceList> response = new ResponseEntity<>(
				resList, HttpStatus.OK);

		return response;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "")
	public ResponseEntity<Void> createHireRequest(
			@RequestBody PlantHireRequestResource res) throws InvalidHirePeriodException, EngineerNotFoundException {
		Planthirerequest p = requestService.createRequest(res);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(linkTo(RESTController.class).slash( 
				 p.getId()).toUri());

		ResponseEntity<Void> response = new ResponseEntity<>(headers,
				HttpStatus.CREATED);
		return response;
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "")
	public ResponseEntity<Void> modifyHireRequest(
			@RequestBody PlantHireRequestResource res) throws InvalidHirePeriodException, EngineerNotFoundException, RequestNotFoundException {
		Planthirerequest p = requestService.modifyRequest(res);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(linkTo(RESTController.class).slash( 
				 p.getId()).toUri());


		ResponseEntity<Void> response = new ResponseEntity<>(headers,
				HttpStatus.OK);
		return response;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/reject")
	public ResponseEntity<Void> rejectHireRequest(
			@RequestBody RejectBean bean) throws RequestNotFoundException, MethodNotAllowedException {
		Planthirerequest req = requestService.rejectRequest(bean);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(linkTo(RESTController.class).slash( 
				 req.getId()).toUri());

		ResponseEntity<Void> response = new ResponseEntity<>(headers,
				HttpStatus.OK);
		return response;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/approve/{id}")
	public ResponseEntity<PurchaseOrderResource> approveHireRequest(
			@PathVariable Long id) throws RequestNotFoundException, MethodNotAllowedException {
		Planthirerequest req = requestService.approveRequest(id);

		//HttpHeaders headers = new HttpHeaders();
		//headers.setLocation(linkTo(RESTController.class).slash( 
		//		 req.getId()).toUri());

		PurchaseOrderAssembler assembler = new PurchaseOrderAssembler();
		ResponseEntity<PurchaseOrderResource> response = new ResponseEntity<>(assembler.toResource(req),
				HttpStatus.OK);
		return response;
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public ResponseEntity<Void> closeHireRequest(
			@PathVariable Long id) throws RequestNotFoundException, MethodNotAllowedException {
		Planthirerequest req = requestService.closeRequest(id);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(linkTo(RESTController.class).slash( 
				 req.getId()).toUri());

		ResponseEntity<Void> response = new ResponseEntity<>(headers,
				HttpStatus.OK);
		return response;
	}

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/inuse")
    public ResponseEntity<Void> supplierPHRInUse(@PathVariable Long id)throws RequestNotFoundException,
            MethodNotAllowedException {

        Planthirerequest req = requestService.updateRequestInUse(id);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(linkTo(RESTController.class).slash(req.getId()).toUri());

        ResponseEntity<Void> response = new ResponseEntity<>(headers,HttpStatus.OK);
        return response;

    }


    @RequestMapping(method = RequestMethod.PUT, value = "/{id}/backtosupplier")
    public ResponseEntity<Void> pHRBackToSupplier(@PathVariable Long id)throws RequestNotFoundException,
            MethodNotAllowedException {

        Planthirerequest req = requestService.updateToBackSupplier(id);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(linkTo(RESTController.class).slash(req.getId()).toUri());

        ResponseEntity<Void> response = new ResponseEntity<>(headers,HttpStatus.OK);
        return response;

    }

	
	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public ResponseEntity<PurchaseOrderResource> getRequest(
			@PathVariable Long id) throws RequestNotFoundException, NoSuchMethodException, SecurityException {

		Planthirerequest req = requestService.getRequest(id);
		//PlantHireRequestResourceAssembler assembler = new PlantHireRequestResourceAssembler();
		//PlantHireRequestResource resource = assembler.toResource(req);
		PurchaseOrderAssembler assembler = new PurchaseOrderAssembler();
		PurchaseOrderResource resource = assembler.toResource(req);
		
		switch (req.getStatus()) {
			case APPROVED:
				Method _reject = RESTController.class.getMethod(
						"rejectHireRequest", RejectBean.class);
				Method _accept = RESTController.class.getMethod(
						"approveHireRequest", Long.class);

				String acceptLink = linkTo(_accept, req.getId()).toUri()
						.toString();
				resource.add(new ExtendedLink(acceptLink, "Accept Request", "GET"));

				String rejectLink = linkTo(_reject, req.getId()).toUri()
						.toString();
				resource.add(new ExtendedLink(rejectLink, "Reject Request", "POST"));
				break;
			
			case OPEN:
				Method _close = RESTController.class.getMethod(
						"closeHireRequest", Long.class);

				String closeLink = linkTo(_close, req.getId()).toUri()
						.toString();
				resource.add(new ExtendedLink(closeLink, "Close Request", "DELETE"));
				break;
			case REJECTED:
				Method _modify = RESTController.class.getMethod(
						"modifyHireRequest", PlantHireRequestResource.class);
				String modifyLink = linkTo(_modify, req.getId()).toUri()
						.toString();
				resource.add(new ExtendedLink(modifyLink, "Modify Request", "PUT"));
				break;
			default: break;
			
			
		}
		
		Method _close = RESTController.class.getMethod(
				"closeHireRequest", Long.class);

		String closeLink = linkTo(_close, req.getId()).toUri()
				.toString();
		resource.add(new ExtendedLink(closeLink, "Reject Plant", "DELETE"));
		
		
		Method _inuse = RESTController.class.getMethod(
				"supplierPHRInUse", Long.class);

		String inuseLink = linkTo(_inuse, req.getId()).toUri()
				.toString();
		resource.add(new ExtendedLink(inuseLink, "Deliver Plant", "PUT"));
		
		
		Method _plantReturned = RESTController.class.getMethod(
				"pHRBackToSupplier", Long.class);

		String plantReturnedLink = linkTo(_plantReturned, req.getId()).toUri()
				.toString();
		resource.add(new ExtendedLink(plantReturnedLink, "Return Plant", "PUT"));
		
		ResponseEntity<PurchaseOrderResource> response = new ResponseEntity<>(
				resource, HttpStatus.OK);
		
		return response;
	}
	
	@ExceptionHandler({ InvalidHirePeriodException.class })
	public ResponseEntity<String> handleBadRequest(Exception ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler({ EngineerNotFoundException.class,
		RequestNotFoundException.class })
	public ResponseEntity<String> handleNotFound(Exception ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler({ SecurityException.class })
	public ResponseEntity<String> handleSecurity(Exception ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.FORBIDDEN);
	}

	@ExceptionHandler({ NoSuchMethodException.class })
	public ResponseEntity<String> handleNoMethod(Exception ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_IMPLEMENTED);
	}
	
	@ExceptionHandler({ MethodNotAllowedException.class })
	public ResponseEntity<String> handleMethodNotAllowed(Exception ex) {
		return new ResponseEntity<>(ex.getMessage(), HttpStatus.METHOD_NOT_ALLOWED);
	}
	
}
