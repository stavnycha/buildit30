package ee.ut.controller;

import java.net.URI;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import ee.ut.rest.*;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Controller;
import org.w3c.dom.Document;

import beans.ExtensionBean;
import beans.POModification;
import beans.PlantBean;

import com.fasterxml.jackson.core.JsonProcessingException;

import ee.ut.controller.rentits.RentitController;
import ee.ut.controller.rentits.RentitControllerFabric;
import ee.ut.controller.rentits.RentitServers;
import ee.ut.domain.Invoice;
import ee.ut.domain.InvoiceStatus;
import ee.ut.domain.Planthirerequest;
import ee.ut.rest.PlantResource;
import ee.ut.rest.PlantResourceList;
import ee.ut.rest.PurchaseOrderResource;
import ee.ut.rest.QueriedPlant;
import ee.ut.domain.RequestStatus;

@Controller
public class RentitAPIController {
	
	static public PurchaseOrderResource createPO(PurchaseOrderResource po) throws JsonProcessingException{
		Planthirerequest phr = Planthirerequest.findPlanthirerequest(po.getHireRequestId());
		String server = phr.getServer();
		RentitController controller = RentitControllerFabric.getController(server);
		return controller.createPO(po);
	}
	
	static public PurchaseOrderResource getPO(URI location){
		String server = location.getHost();
		RentitController controller = RentitControllerFabric.getController(server);
		return controller.getPO(location);
	}
	
	static public PurchaseOrderResource getPO(Planthirerequest phr){
		String server = phr.getServer();
		RentitController controller = RentitControllerFabric.getController(server);
        PurchaseOrderResource por = controller.getPO(phr.getPoid());
		return por;
	}
	
	static public PlantResource getPlant(Planthirerequest phr){
		String server = phr.getServer();
		RentitController controller = RentitControllerFabric.getController(server);
		return controller.getPlant(phr.getPlant());
	}
	

	static public void acceptInvoice(Planthirerequest phr){
		String server = phr.getServer();
		RentitController controller = RentitControllerFabric.getController(server);
		controller.acceptInvoice(phr.getInvoice());
	}
	
	static public PlantResourceList getAvailablePlants(QueriedPlant plant, String server) throws JsonProcessingException{
		RentitController controller = RentitControllerFabric.getController(server);
		return controller.getAvailablePlants(plant);
	}
	
	static public boolean cancelPO(Long id){
		Planthirerequest phr = Planthirerequest.findPlanthirerequest(id);
		String server = phr.getServer();
		RentitController controller = RentitControllerFabric.getController(server);
		return controller.cancelPO(id);
	}
	
	static public PurchaseOrderResource modifyPO(POModification m) throws JsonProcessingException{
		Planthirerequest phr = Planthirerequest.findPlanthirerequest(m.getId());
		String server = phr.getServer();
		RentitController controller = RentitControllerFabric.getController(server);
		return controller.modifyPO(m);
	}
	
	static public void requestUpdate(ExtensionBean bean) throws JsonProcessingException{
		
		Planthirerequest phr = Planthirerequest.findPlanthirerequest(bean.getId());
		String server = phr.getServer();
		RentitController controller = RentitControllerFabric.getController(server);
		controller.requestUpdate(bean);
	}

	static private RentitController getControllerFromInvocie(Document invoice){
		XPath xPath = XPathFactory.newInstance().newXPath();
		Planthirerequest phr;
			try {
				String requestId = xPath.evaluate("//requestId", invoice);
				
				if (requestId == ""){
					String email = xPath.evaluate("//rentitEmailAddress", invoice);
					String server = RentitServers.getServerFromEmail(email);
					String po = xPath.evaluate("//purchaseOrderId", invoice);
					phr = Planthirerequest.findPlanthirerequestByPOId(Long.parseLong(po), server);
				} else {
					phr = Planthirerequest.findPlanthirerequest(Long.parseLong(requestId));
				}
				
				String server = phr.getServer();
				return RentitControllerFabric.getController(server);
			} catch (XPathExpressionException e) {
				e.printStackTrace();
				return null;
			} 
	}
	
	public static Double getPriceFromInvoice(Document invoice) throws JAXBException, XPathExpressionException {
		RentitController controller = getControllerFromInvocie(invoice);
		return controller.getPriceFromInvoice(invoice);
	}

	public static InvoiceResource getInvoice(Document invoiceXML) throws JAXBException, ParseException {
		RentitController controller = getControllerFromInvocie(invoiceXML);
		return controller.getInvoice(invoiceXML);
	}

	public static List<PlantBean> getAllPlants() {
		RentitServers servers = new RentitServers();
		List<PlantBean> finalList = new ArrayList<>();
		for (String server: servers.getAllServers()){
			RentitController controller = RentitControllerFabric.getController(server);
			List<PlantBean> temp = new ArrayList<>();
			PlantResourceList plants = controller.getAllPlants();
			for (PlantResource p: plants.getPlants()){
				PlantBean pb = new PlantBean();
				pb.setDescription(p.getDescription());
				pb.setPlantId(p.getPlantId());
				pb.setPrice(p.getPrice());
				pb.setTitle(p.getTitle());
				pb.setServerPlant(server + "::" + p.getPlantId());
				temp.add(pb);
			}
			if(!temp.isEmpty()){
	             finalList.addAll(temp);
	         }
		}
		return finalList;
	}
}
