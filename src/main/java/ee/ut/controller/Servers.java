package ee.ut.controller;

import java.util.Arrays;

import org.springframework.http.HttpHeaders;
import org.springframework.security.crypto.codec.Base64;

public class Servers {

	static public String buildit = //"http://localhost:8080/Buildit";//*/
			"http://buildit30.herokuapp.com";
	static public String email = "esi.buildit@gmail.com";
	static public String credentials = "rentit:rentit";
	
	static public HttpHeaders getHeaders(String auth) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(org.springframework.http.MediaType.APPLICATION_JSON);
		headers.setAccept(Arrays.asList(org.springframework.http.MediaType.APPLICATION_JSON));
		byte[] encodedAuthorisation = Base64.encode(auth.getBytes());
		headers.add("Authorization", "Basic " + new String(encodedAuthorisation));
		System.out.println("---- " + new String(encodedAuthorisation));
		return headers;
	}
}
