package ee.ut.domain;

public enum RequestStatus {

    PENDING_CONFIRMATION,
    APPROVED,
    PENDING_PLANT_EXAMINATION,
    OPEN,
    DECLINED,
    CLOSED,
    REJECTED,
    RETURNED,
    PLANT_IN_USE,
    PLANT_BACK_TO_SUPPLIER   
}
