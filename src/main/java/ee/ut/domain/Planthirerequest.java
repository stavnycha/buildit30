package ee.ut.domain;
import java.util.Calendar;
import java.util.List;

import javax.persistence.*;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import ee.ut.rest.POStatus;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import ee.ut.utils.DateAdapter;


@RooJavaBean
@RooToString
@RooJpaActiveRecord(versionField = "", table = "planthirerequest", schema = "public", finders = { "findPlanthirerequestsBySiteEngineer", "findPlanthirerequestsByInvoice", "findPlanthirerequestsByPoidEquals" })
public class Planthirerequest {

    
    @ManyToOne
    private Workengineer workEngineer;

    /**
     */
    @ManyToOne
    private Siteengineer siteEngineer;

    /**
     */
    @OneToOne
    private RequestUpdate update;
   

    public RequestUpdate getUpdate() {
        return update;
    }

    public void setUpdate(RequestUpdate update) {
        this.update = update;
    }

    @Column(name = "siteid")
    private Integer siteId;

    @Column(name = "plant")
    private Integer plant;

    @Column(name = "price")
    private Double price;

    @Column(name = "poid")
    private Long poid;

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Column(name = "server")
    private String server;

    @Column(name = "startdate")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Calendar startDate;

    @Column(name = "enddate")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Calendar endDate;

    @Column(name = "status")
    private RequestStatus status;

    @Column(name = "comment")
    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getSiteId() {
        return siteId;
    }

    public void setSiteId(Integer siteid) {
        this.siteId = siteid;
    }

    @XmlJavaTypeAdapter(DateAdapter.class)
    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(Calendar startdate) {
        this.startDate = startdate;
    }

    @XmlJavaTypeAdapter(DateAdapter.class)
    public Calendar getEndDate() {
        return endDate;
    }

    public void setEndDate(Calendar enddate) {
        this.endDate = enddate;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus s) {
        this.status = s;
    }

    /**
     */
    @OneToOne
    private Invoice invoice;

    public static List<Planthirerequest> findPlanthirerequestEntriesBySiteEng(int firstResult, int maxResults, Siteengineer se) {
        return entityManager().createQuery("SELECT o FROM Planthirerequest o where siteengineer = " + se.getId(), Planthirerequest.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }

}
