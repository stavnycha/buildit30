// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package ee.ut.domain;

import ee.ut.domain.Siteengineer;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Siteengineer_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager Siteengineer.entityManager;
    
    public static final EntityManager Siteengineer.entityManager() {
        EntityManager em = new Siteengineer().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Siteengineer.countSiteengineers() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Siteengineer o", Long.class).getSingleResult();
    }
    
    public static List<Siteengineer> Siteengineer.findAllSiteengineers() {
        return entityManager().createQuery("SELECT o FROM Siteengineer o", Siteengineer.class).getResultList();
    }
    
    public static Siteengineer Siteengineer.findSiteengineer(Long id) {
        if (id == null) return null;
        return entityManager().find(Siteengineer.class, id);
    }
    
    public static List<Siteengineer> Siteengineer.findSiteengineerEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Siteengineer o", Siteengineer.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void Siteengineer.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Siteengineer.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Siteengineer attached = Siteengineer.findSiteengineer(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Siteengineer.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Siteengineer.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Siteengineer Siteengineer.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Siteengineer merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
