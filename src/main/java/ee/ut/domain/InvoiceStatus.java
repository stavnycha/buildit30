package ee.ut.domain;

public enum InvoiceStatus {
	PAID, NOT_PAID
}
