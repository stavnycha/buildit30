package ee.ut.domain;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooJpaActiveRecord(versionField = "", table = "invoice", schema = "public")
public class Invoice {

	@Column(name = "date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Date date;
	
	@Column(name = "status")
	private InvoiceStatus status;
	
	@Column(name = "total")
	private Double total;
	
	@Column(name = "invoicenumber")
	private Long invoiceNumber;
	

	@Override
	public String toString(){
		return status + " " + total;
	}
}
