package ee.ut.domain;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import ee.ut.utils.DateAdapter;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(versionField = "", table = "requestupdate", schema = "public")
public class RequestUpdate {
	@Column(name = "enddate")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
	private Calendar endDate;
	
	private URStatus status;

	@XmlJavaTypeAdapter(DateAdapter.class)
	public Calendar getEndDate() {
        return this.endDate;
    }

	public void setEndDate(Calendar endDate) {
        this.endDate = endDate;
    }

	public URStatus getStatus() {
        return this.status;
    }

	public void setStatus(URStatus status) {
        this.status = status;
    }
}
