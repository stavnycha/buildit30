// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package ee.ut.domain;

import ee.ut.domain.Workengineer;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

privileged aspect Workengineer_Roo_Finder {
    
    public static TypedQuery<Workengineer> Workengineer.findWorkengineersByEmailEquals(String email) {
        if (email == null || email.length() == 0) throw new IllegalArgumentException("The email argument is required");
        EntityManager em = Workengineer.entityManager();
        TypedQuery<Workengineer> q = em.createQuery("SELECT o FROM Workengineer AS o WHERE o.email = :email", Workengineer.class);
        q.setParameter("email", email);
        return q;
    }
    
}
