// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package ee.ut.domain;

import ee.ut.domain.Invoice;
import ee.ut.domain.Planthirerequest;
import ee.ut.domain.Siteengineer;
import ee.ut.domain.Workengineer;

privileged aspect Planthirerequest_Roo_JavaBean {
    
    public Workengineer Planthirerequest.getWorkEngineer() {
        return this.workEngineer;
    }
    
    public void Planthirerequest.setWorkEngineer(Workengineer workEngineer) {
        this.workEngineer = workEngineer;
    }
    
    public Siteengineer Planthirerequest.getSiteEngineer() {
        return this.siteEngineer;
    }
    
    public void Planthirerequest.setSiteEngineer(Siteengineer siteEngineer) {
        this.siteEngineer = siteEngineer;
    }
    
    public Integer Planthirerequest.getPlant() {
        return this.plant;
    }
    
    public void Planthirerequest.setPlant(Integer plant) {
        this.plant = plant;
    }
    
    public Long Planthirerequest.getPoid() {
        return this.poid;
    }
    
    public void Planthirerequest.setPoid(Long poid) {
        this.poid = poid;
    }
    
    public String Planthirerequest.getServer() {
        return this.server;
    }
    
    public void Planthirerequest.setServer(String server) {
        this.server = server;
    }
    
    public Invoice Planthirerequest.getInvoice() {
        return this.invoice;
    }
    
    public void Planthirerequest.setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
    
}
