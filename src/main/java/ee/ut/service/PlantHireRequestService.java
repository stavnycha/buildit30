package ee.ut.service;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.mail.MessagingException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import ee.ut.rest.InvoiceResource;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import beans.ExtensionBean;

import com.fasterxml.jackson.core.JsonProcessingException;

import ee.ut.controller.RentitAPIController;
import ee.ut.controller.Servers;
import ee.ut.controller.rentits.RentitController;
import ee.ut.controller.rentits.RentitControllerFabric;
import ee.ut.controller.rentits.RentitServers;
import ee.ut.domain.Invoice;
import ee.ut.domain.InvoiceStatus;
import ee.ut.domain.Planthirerequest;
import ee.ut.domain.RequestStatus;
import ee.ut.domain.Siteengineer;
import ee.ut.domain.Workengineer;
import ee.ut.exceptions.EngineerNotFoundException;
import ee.ut.exceptions.InvalidHirePeriodException;
import ee.ut.exceptions.InvoiceNotValidException;
import ee.ut.exceptions.MethodNotAllowedException;
import ee.ut.exceptions.RequestNotFoundException;
import ee.ut.rest.PlantHireRequestResource;
import ee.ut.rest.PlantResource;
import ee.ut.rest.PurchaseOrderResource;
import ee.ut.rest.RejectBean;

@Service
public class PlantHireRequestService {

	@Autowired
	protected JavaMailSender mailSender;

	public void setPrice(Planthirerequest req) {
		DateTime startDate = new DateTime(req.getStartDate());
		DateTime endDate = new DateTime(req.getEndDate());
		int days = Days.daysBetween(startDate, endDate).getDays();

		/*
		 * Authentication authentication =
		 * SecurityContextHolder.getContext().getAuthentication(); String name =
		 * authentication.getName(); Siteengineer se =
		 * Siteengineer.findSiteengineersByEmailEquals(name).getSingleResult();
		 */

		PlantResource plant = RentitAPIController.getPlant(req);
		/*
		 * HttpEntity<String> requestEntity = new
		 * HttpEntity<String>(Servers.getHeaders("buildit:buildit"));
		 * RestTemplate template = new RestTemplate();
		 * ResponseEntity<PlantResource> response = template.exchange(server +
		 * "/rest/plants/" + req.getPlant(), HttpMethod.GET, requestEntity,
		 * PlantResource.class); PlantResource plant = response.getBody();
		 */

		/*
		 * Client client = Client.create(); WebResource webResource =
		 * client.resource(server + "/rest/plants/" + req.getPlant());
		 * PlantResource plant = webResource.type(MediaType.APPLICATION_XML)
		 * .accept
		 * (MediaType.APPLICATION_XML).get(ClientResponse.class).getEntity
		 * (PlantResource.class);
		 */
		req.setPrice(days * plant.getPrice());
	}

	public Planthirerequest createRequest(PlantHireRequestResource res)
			throws InvalidHirePeriodException, EngineerNotFoundException {

		if (res.getStartDate().compareTo(res.getEndDate()) >= 0)
			throw new InvalidHirePeriodException("Invalid date range");

		Planthirerequest p = new Planthirerequest();
		p.setStatus(RequestStatus.PENDING_CONFIRMATION);
		p.setEndDate(res.getEndDate());
		p.setStartDate(res.getStartDate());
		p.setComment(res.getComment());

		Siteengineer site = res.getSiteEngineer() == null ? null : Siteengineer
				.findSiteengineer(res.getSiteEngineer().getEngineerId());
		if (site == null && res.getSiteEngineer() != null)
			throw new EngineerNotFoundException("Site engineer not found.");
		Workengineer work = res.getWorkEngineer() == null ? null : Workengineer
				.findWorkengineer(res.getWorkEngineer().getWorkId());
		if (work == null && res.getWorkEngineer() != null)
			throw new EngineerNotFoundException("Work engineer not found.");
		p.setSiteEngineer(site);
		p.setWorkEngineer(work);
		p.setSiteId(res.getSiteId());
		p.setPlant(res.getPlant());
		setPrice(p);
		p.persist();
		return p;
	}

	public Planthirerequest modifyRequest(PlantHireRequestResource res)
			throws InvalidHirePeriodException, EngineerNotFoundException,
			RequestNotFoundException {

		Planthirerequest p = Planthirerequest.findPlanthirerequest(res
				.getHireRequestId());
		if (p == null)
			throw new RequestNotFoundException("Request not found.");
		if (res.getStartDate().compareTo(res.getEndDate()) >= 0)
			throw new InvalidHirePeriodException("Invalid date range");

		p.setStatus(res.getStatus());
		p.setEndDate(res.getEndDate());
		p.setStartDate(res.getStartDate());
		p.setComment(res.getComment());
		p.setPrice(res.getCost());
		Siteengineer site = res.getSiteEngineer() != null ? Siteengineer
				.findSiteengineer(res.getSiteEngineer().getEngineerId()) : null;
		if (site == null)
			throw new EngineerNotFoundException("Site engineer not found.");
		Workengineer work = res.getWorkEngineer() != null ? Workengineer
				.findWorkengineer(res.getWorkEngineer().getWorkId()) : null;
		if (work == null)
			throw new EngineerNotFoundException("Work engineer not found.");
		p.setSiteEngineer(site);
		p.setWorkEngineer(work);
		p.setSiteId(res.getSiteId());
		p.setPlant(res.getPlant());
		p.persist();
		return p;
	}

	public Planthirerequest getRequest(Long id) throws RequestNotFoundException {
		Planthirerequest req = Planthirerequest.findPlanthirerequest(id);
		if (req == null)
			throw new RequestNotFoundException("Plant Hire Request not found.");
		return req;
	}

	/*
	 * public Planthirerequest confirmRequest(Long id) throws
	 * RequestNotFoundException, MethodNotAllowedException { Planthirerequest
	 * req = Planthirerequest.findPlanthirerequest(id); if (req == null) throw
	 * new RequestNotFoundException("Plant Hire Request not found."); if
	 * (req.getStatus() != RequestStatus.DECLINED && req.getStatus() !=
	 * RequestStatus.PENDING_PLANT_EXAMINATION) throw new
	 * MethodNotAllowedException("Method is not allowed.");
	 * req.setStatus(RequestStatus.OPEN); req.persist(); return req; }
	 */

	public Planthirerequest approveRequest(Long id)
			throws RequestNotFoundException, MethodNotAllowedException {
		Planthirerequest req = Planthirerequest.findPlanthirerequest(id);
		if (req == null)
			throw new RequestNotFoundException("Plant Hire Request not found.");
		if (req.getStatus() != RequestStatus.APPROVED)
			throw new MethodNotAllowedException("Method is not allowed.");
		req.setStatus(RequestStatus.PENDING_PLANT_EXAMINATION);
		req.persist();
		return req;
	}

	/*
	 * public Planthirerequest declineRequest(Long id) throws
	 * RequestNotFoundException, MethodNotAllowedException { Planthirerequest
	 * req = Planthirerequest.findPlanthirerequest(id); if (req == null) throw
	 * new RequestNotFoundException("Plant Hire Request not found."); if
	 * (req.getStatus() != RequestStatus.DECLINED && req.getStatus() !=
	 * RequestStatus.PENDING_PLANT_EXAMINATION) throw new
	 * MethodNotAllowedException("Method is not allowed.");
	 * req.setStatus(RequestStatus.DECLINED); req.persist(); return req; }
	 */

	public Planthirerequest acceptRequest(Long id)
			throws RequestNotFoundException, MethodNotAllowedException {
		Planthirerequest req = Planthirerequest.findPlanthirerequest(id);
		if (req == null)
			throw new RequestNotFoundException("Plant Hire Request not found.");
		if (req.getStatus() != RequestStatus.REJECTED
				&& req.getStatus() != RequestStatus.PENDING_CONFIRMATION)
			throw new MethodNotAllowedException("Method is not allowed.");

		return req;
	}

	public Planthirerequest rejectRequest(RejectBean bean)
			throws RequestNotFoundException, MethodNotAllowedException {
		Planthirerequest req = Planthirerequest.findPlanthirerequest(bean
				.getId());
		if (req == null)
			throw new RequestNotFoundException("Plant Hire Request not found.");
		if (req.getStatus() != RequestStatus.APPROVED)
			throw new MethodNotAllowedException("Method is not allowed.");
		req.setStatus(RequestStatus.REJECTED);
		req.setComment(bean.getComment());
		req.persist();
		return req;
	}

	public Planthirerequest updateRequestInUse(Long id)
			throws RequestNotFoundException, MethodNotAllowedException {
		Planthirerequest req = Planthirerequest.findPlanthirerequest(id);
		if (req == null)
			throw new RequestNotFoundException("Plant Hire Request not found.");

		req.setStatus(RequestStatus.PLANT_IN_USE);
		req.persist();
		return req;
	}

	public Planthirerequest updateToBackSupplier(Long id)
			throws RequestNotFoundException, MethodNotAllowedException {
		Planthirerequest req = Planthirerequest.findPlanthirerequest(id);
		if (req == null) {
			throw new RequestNotFoundException("Plant Hire Request not found.");
		}
		if (RequestStatus.PLANT_IN_USE.equals(req.getStatus())) {
			req.setStatus(RequestStatus.PLANT_BACK_TO_SUPPLIER);
		}
		req.persist();
		return req;
	}

	public Planthirerequest closeRequest(Long id)
			throws RequestNotFoundException, MethodNotAllowedException {
		Planthirerequest req = Planthirerequest.findPlanthirerequest(id);
		if (req == null)
			throw new RequestNotFoundException("Plant Hire Request not found.");
		if (req.getStatus() != RequestStatus.OPEN
				&& req.getStatus() != RequestStatus.PENDING_PLANT_EXAMINATION)
			throw new MethodNotAllowedException("Method is not allowed.");
		req.setStatus(RequestStatus.CLOSED);
		req.persist();
		return req;
	}

	/*
	 * public Planthirerequest createUpdateRequest(Long id,
	 * RequestUpdateResource res) throws RequestNotFoundException,
	 * InvalidHirePeriodException{ Planthirerequest req =
	 * Planthirerequest.findPlanthirerequest(id); if (req == null) throw new
	 * RequestNotFoundException("Plant Hire Request not found."); if
	 * (req.getEndDate().compareTo(res.getDate()) >= 0) throw new
	 * InvalidHirePeriodException("Invalid date range"); RequestUpdate update =
	 * new RequestUpdate(); update.setEndDate(res.getDate());
	 * update.setStatus(URStatus.OPEN); req.setUpdate(update); req.persist();
	 * return req; }
	 */

	public void extendPO(ExtensionBean bean) throws JsonProcessingException {
		Planthirerequest phr = Planthirerequest.findPlanthirerequest(bean
                .getId());
		if (bean.getEndDate().after(phr.getEndDate().getTime())) {
			RentitAPIController.requestUpdate(bean);
		} else {
			phr.setComment("Extension is rejected, end date is wrong");
		}
	}

	public boolean cancelPO(Long id) {
		Planthirerequest phr = Planthirerequest.findPlanthirerequest(id);
		
		Calendar today = Calendar.getInstance();
		today.setTime(new Date());
		today.add(Calendar.DATE, 1);
			
		Calendar deliveringDate = Calendar.getInstance(); 
		deliveringDate.setTime(phr.getStartDate().getTime()); 
		
		if (today.before(deliveringDate))
			return RentitAPIController.cancelPO(id);
		else
			return false;
	}
	
	public void payInvoice(InvoiceResource i){
		Planthirerequest phr = Planthirerequest.findPlanthirerequest(i.getRequestId());
		String server = phr.getServer();
		RentitController controller = RentitControllerFabric.getController(server);
		String remittanceAdvice = controller.getRemittanceAdvice(i);
		
		phr = Planthirerequest.findPlanthirerequest(i.getRequestId());
		controller.acceptInvoice(phr.getInvoice());
		
		phr.setStatus(RequestStatus.CLOSED);
		phr.persist();
		phr.getInvoice().setStatus(InvoiceStatus.PAID);
		phr.getInvoice().merge();
		
		SimpleMailMessage mailMessage = new SimpleMailMessage();

		mailMessage.setTo(controller.getEmail());
		mailMessage.setSentDate(new Date());
		mailMessage.setSubject("Remmitance Advice");

		mailMessage.setText(remittanceAdvice);

		mailSender.send(mailMessage);
		
	}

}
