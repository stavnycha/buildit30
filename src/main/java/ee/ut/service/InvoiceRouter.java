package ee.ut.service;

import javax.xml.bind.JAXBException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;

import ee.ut.controller.RentitAPIController;

public class InvoiceRouter {
	// Comparing invoice's total with its corresponding po's total
	public String analyzeInvoice(Document invoice)
			throws XPathExpressionException, JAXBException {
		String destinationChannel = null;
		
		Double price = RentitAPIController.getPriceFromInvoice(invoice);
		if (price <= 100)
			destinationChannel = "MINOR";
		else
			destinationChannel = "MAJOR";
		return destinationChannel;
	}
	
	
}
