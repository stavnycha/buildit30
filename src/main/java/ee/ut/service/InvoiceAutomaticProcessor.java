package ee.ut.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ee.ut.controller.RentitAPIController;
import ee.ut.rest.InvoiceResource;

@Component
public class InvoiceAutomaticProcessor extends InvoiceProcessor {

	@Autowired 
	private PlantHireRequestService requestService;


	@Override
	public void processInvoice(InvoiceResource invoice) {
		saveInvoice(invoice);
		requestService.payInvoice(invoice);
	}

}
