package ee.ut.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import ee.ut.controller.RentitAPIController;
import ee.ut.controller.Servers;
import ee.ut.controller.rentits.RentitServers;
import ee.ut.domain.Planthirerequest;
import ee.ut.exceptions.InvoiceNotValidException;
import ee.ut.rest.InvoiceResource;
import ee.ut.rest.PurchaseOrderResource;
import ee.ut.soap.client.Invoice;

@Component
abstract public class InvoiceProcessor {

	@Autowired
	protected JavaMailSender mailSender;

	abstract public void processInvoice(InvoiceResource invoice);

	@ServiceActivator
	public Document process(Document invoiceXML) throws XPathExpressionException, IOException, MessagingException, ParserConfigurationException, SAXException, JAXBException, ParseException {
		InvoiceResource invoice = RentitAPIController.getInvoice(invoiceXML);
		try {
			checkPOValidity(invoice);
		} catch (InvoiceNotValidException e) {
			SimpleMailMessage mailMessage = new SimpleMailMessage();
			Planthirerequest phr = Planthirerequest.findPlanthirerequest(invoice.getRequestId());
			
			mailMessage.setTo(RentitServers.getEmailFromServer(phr.getServer()));
			mailMessage.setSentDate(new Date());
			mailMessage.setSubject("Response to invoice");
			
			mailMessage.setText("Invoice for Purchase Order Id:-" + invoice.getPoId() + "-\n" + 
					"Invoice Id:-" + invoice.getInvoiceId() + "- is not valid. " + e.getMessage());

			mailSender.send(mailMessage);
		}
		processInvoice(invoice);
		return invoiceXML;
	}

	public ee.ut.domain.Invoice saveInvoice(InvoiceResource invoice) {
		ee.ut.domain.Invoice obj = new ee.ut.domain.Invoice();
		obj.setDate(invoice.getDate());
		obj.setStatus(invoice.getStatus());
		obj.setTotal(invoice.getTotal());
		obj.setInvoiceNumber(invoice.getInvoiceId());
		Planthirerequest req = Planthirerequest.findPlanthirerequest(invoice
				.getRequestId());

		obj.persist();
		req.setInvoice(obj);
		req.merge();
		return obj;
	}

	public void checkPOValidity(InvoiceResource i) throws IOException,
			MessagingException, ParserConfigurationException, SAXException,
			XPathExpressionException, InvoiceNotValidException {

		if (i == null) {
			throw new InvoiceNotValidException("Invalid invoice format.");
		}

		Planthirerequest req = Planthirerequest.findPlanthirerequest(i
				.getRequestId());
		PurchaseOrderResource res = RentitAPIController.getPO(req);
		if (res == null) {
			throw new InvoiceNotValidException("Purchase Order Not Found.");
		}
		if (req.getPrice().doubleValue() != res.getPrice().doubleValue()) {
			throw new InvoiceNotValidException(
					"The cost in invoice is not identical to the cost in hire request. "
							+ "Invoice:" + res.getPrice() + ", Our system: "
							+ req.getPrice());
		}
	}
}
