package ee.ut.service;

import org.springframework.stereotype.Component;

import ee.ut.rest.InvoiceResource;

@Component
public class InvoiceHumanAssistedHandling extends InvoiceProcessor {

	@Override
	public void processInvoice(InvoiceResource invoice) {
		saveInvoice(invoice);
	}
}
