package ee.ut.rest;

import java.util.Calendar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.roo.addon.javabean.RooJavaBean;

import ee.ut.domain.RequestStatus;
import ee.ut.domain.RequestUpdate;
import ee.ut.utils.ResourceSupport;

@RooJavaBean
@XmlRootElement(name = "request")
@XmlAccessorType(XmlAccessType.FIELD)
public class PlantHireRequestResource extends ResourceSupport  {
	
	private Long hireRequestId;
	private Long poId;
	private Integer plant;
	private WorkEngineerResource workEngineer;
	private SiteEngineerResource siteEngineer;
	private Integer siteId;

	private Calendar startDate;
	private Calendar endDate;

	private RequestStatus status;
	private String comment;
	private Double cost;
	private String server;
}
