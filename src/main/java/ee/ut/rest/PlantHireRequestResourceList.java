package ee.ut.rest;

import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.roo.addon.javabean.RooJavaBean;

@RooJavaBean
@XmlRootElement(name = "requests")
public class PlantHireRequestResourceList {
	private List<PlantHireRequestResource> requests;
	
	public PlantHireRequestResourceList () { 
		requests = new LinkedList<PlantHireRequestResource>();
	}
	public PlantHireRequestResourceList (List<PlantHireRequestResource> list){
		requests = list;
	}

	@XmlElement(name = "request")
	public List<PlantHireRequestResource> getRequests() {
        return this.requests;
    }

	public void setRequests(List<PlantHireRequestResource> requests) {
        this.requests = requests;
    }
}
