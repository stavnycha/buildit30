package ee.ut.rest.rentit9;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

import org.springframework.roo.addon.javabean.RooJavaBean;

@RooJavaBean
@XmlRootElement(name="plant")
@XmlAccessorType(XmlAccessType.FIELD)
public class PlantResource {

    private Long id;
        private String name;
        private String description;
        private Float price;

}
