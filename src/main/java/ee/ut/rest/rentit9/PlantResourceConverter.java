package ee.ut.rest.rentit9;

import java.util.LinkedList;
import java.util.List;

public class PlantResourceConverter {

	public ee.ut.rest.PlantResource toResource(PlantResource p){
		ee.ut.rest.PlantResource res = new ee.ut.rest.PlantResource();
		res.setDescription(p.getDescription());
		res.setPlantId(p.getId().intValue());
		res.setPrice(p.getPrice().doubleValue());
		res.setTitle(p.getName());
		return res;
	}
	
	public ee.ut.rest.PlantResourceList toResource(PlantResourceList plist){
		ee.ut.rest.PlantResourceList res = new ee.ut.rest.PlantResourceList();
		List<ee.ut.rest.PlantResource> plants = new LinkedList<ee.ut.rest.PlantResource>();
		for (int i = 0; i < plist.getPlants().size(); i++) {
			plants.add(toResource(plist.getPlants().get(i)));
		}
		res.setPlants(plants);
		return res;
	}
	
	public PlantResource fromResource(ee.ut.rest.PlantResource p) throws Exception{
		throw new Exception("not implemented---");
	}
}
