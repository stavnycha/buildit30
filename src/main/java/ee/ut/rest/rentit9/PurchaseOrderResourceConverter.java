package ee.ut.rest.rentit9;

import java.util.Calendar;

public class PurchaseOrderResourceConverter {

	public ee.ut.rest.PurchaseOrderResource toResource(PurchaseOrderResource po){
		PlantResourceConverter conv = new PlantResourceConverter();
		ee.ut.rest.PurchaseOrderResource res = new ee.ut.rest.PurchaseOrderResource();
		res.setEnd(po.getEndDate().getTime());
		res.setStart(po.getStartDate().getTime());
		ee.ut.rest.PlantResource plant = new ee.ut.rest.PlantResource();
		plant.setTitle(po.getPlantName());
		plant.setPlantId(Integer.parseInt(po.getPlantId()));
		res.setPlant(plant);
		res.setPoId(Long.parseLong(po.getRentitOrderId()));
		res.setPrice(po.getTotal().doubleValue());
		POStatus s = new POStatus();
		res.setStatus(s.convert(po.getStatus()));
		res.setHireRequestId(Long.parseLong(po.getBuilditOrderId()));
		return res;
	}
	
	public PurchaseOrderResource fromResource(ee.ut.rest.PurchaseOrderResource po){
		PurchaseOrderResource res = new PurchaseOrderResource();
		res.setBuildit("buildit30");
		res.setBuilditOrderId(po.getHireRequestId().toString());
		Calendar c1 = Calendar.getInstance(), c2 = Calendar.getInstance();
		c1.setTime(po.getEnd());
		c2.setTime(po.getStart());
		res.setEndDate(c1);
		res.setStartDate(c2);
		res.setPlantId(po.getPlant().getPlantId().toString());
		res.setPlantName(po.getPlant().getTitle());
		res.setSiteAddress(po.getSiteId().toString());
		if (po.getPrice() != null)
			res.setTotal(po.getPrice().floatValue());
		//POStatus s = new POStatus();
		//res.setStatus(s.convertFrom(po.getStatus()));
		return res;
	}
	
	
}
