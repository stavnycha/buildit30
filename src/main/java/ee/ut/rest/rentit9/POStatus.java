package ee.ut.rest.rentit9;

public class POStatus {

	public ee.ut.rest.POStatus convert(String status){
		switch (status){
		case "CREATED":
			return ee.ut.rest.POStatus.PENDING_CONFIRMATION; 
		case "APPROVED":
			return ee.ut.rest.POStatus.OPEN; 
		case "REJECTED":
			return ee.ut.rest.POStatus.REJECTED; 
		case "CANCELLED":
			return ee.ut.rest.POStatus.CANCELLED; 
		case "PLANT_DISPATCHED":
			return ee.ut.rest.POStatus.PLANT_DISPATCHED; 
		case "PLANT_DELIVERED":
			return ee.ut.rest.POStatus.PLANT_DELIVERED; 
		case "PLANT_RETURNED":
			return ee.ut.rest.POStatus.PLANT_RETURNED; 
		case "INVOICED":
			return ee.ut.rest.POStatus.INVOICED; 
		default:
			return ee.ut.rest.POStatus.CLOSED; 
		}
	}
	
	public String convertFrom(ee.ut.rest.POStatus status){
		switch (status){
		case PENDING_CONFIRMATION:
			return "CREATED"; 
		case OPEN:
			return "APPROVED"; 
		case REJECTED:
			return "REJECTED"; 
		case CANCELLED:
			return "CANCELLED"; 
		case PLANT_DISPATCHED:
			return "PLANT_DISPATCHED"; 
		case PLANT_DELIVERED:
			return "PLANT_DELIVERED"; 
		case PLANT_RETURNED:
			return "PLANT_RETURNED"; 
		case INVOICED:
			return "INVOICED"; 
		default:
			return "COMPLETED"; 
		}
	}
}
