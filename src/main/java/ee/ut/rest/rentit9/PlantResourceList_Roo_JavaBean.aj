// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package ee.ut.rest.rentit9;

import ee.ut.rest.rentit9.PlantResource;
import ee.ut.rest.rentit9.PlantResourceList;
import java.util.List;

privileged aspect PlantResourceList_Roo_JavaBean {
    
    public List<PlantResource> PlantResourceList.getPlants() {
        return this.plants;
    }
    
    public void PlantResourceList.setPlants(List<PlantResource> plants) {
        this.plants = plants;
    }
    
}
