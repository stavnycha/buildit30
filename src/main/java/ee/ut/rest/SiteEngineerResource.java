package ee.ut.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.roo.addon.javabean.RooJavaBean;
import ee.ut.utils.ResourceSupport;

@RooJavaBean
@XmlRootElement(name = "siteengineer")
@XmlAccessorType(XmlAccessType.FIELD)
public class SiteEngineerResource  extends ResourceSupport {
	private String name;
	private String email;
	private Long engineerId;

	public String getName() {
        return this.name;
    }

	public void setName(String name) {
        this.name = name;
    }

	public String getEmail() {
        return this.email;
    }

	public void setEmail(String email) {
        this.email = email;
    }

	public Long getEngineerId() {
        return this.engineerId;
    }

	public void setEngineerId(Long engineerId) {
        this.engineerId = engineerId;
    }
}
