package ee.ut.rest;

import java.util.LinkedList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import ee.ut.controller.RESTController;
import ee.ut.domain.Planthirerequest;

public class PlantHireRequestResourceAssembler 
extends ResourceAssemblerSupport<Planthirerequest, PlantHireRequestResource>{
	
	public PlantHireRequestResourceAssembler () { 
		 super(RESTController.class, PlantHireRequestResource.class); 
	}

	
	public PlantHireRequestResourceList toResource (List<Planthirerequest> requests) {
		List<PlantHireRequestResource> list = new LinkedList<PlantHireRequestResource>();
		for (int i = 0; i < requests.size(); i++)
			list.add(toResource(requests.get(i)));
		return new PlantHireRequestResourceList(list);
	}
	
	@Override
	public PlantHireRequestResource toResource(Planthirerequest req) {
		SiteEngineerResourceAssembler seassembler = new SiteEngineerResourceAssembler();
		WorkEngineerResourceAssembler weassembler = new WorkEngineerResourceAssembler();
		PlantHireRequestResource res  = createResourceWithId(req.getId(), req);
		res.setHireRequestId(req.getId());
		res.setStatus(req.getStatus());
		res.setSiteEngineer(req.getSiteEngineer() == null ? null : seassembler.toResource(req.getSiteEngineer()));
		res.setWorkEngineer(req.getWorkEngineer() == null ? null : weassembler.toResource(req.getWorkEngineer()));
		
		res.setSiteId(req.getSiteId());
		res.setStartDate(req.getStartDate());
		res.setEndDate(req.getEndDate());
		res.setPlant(req.getPlant());
		res.setComment(req.getComment());
		res.setCost(req.getPrice());
		res.setPoId(req.getPoid());
		res.setServer(req.getServer());
		
		return res;
	}
}
