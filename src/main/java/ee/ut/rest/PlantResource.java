package ee.ut.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.roo.addon.javabean.RooJavaBean;

import ee.ut.utils.ResourceSupport;

@RooJavaBean
@XmlRootElement(name = "plant")
@XmlAccessorType(XmlAccessType.FIELD)
public class PlantResource  extends ResourceSupport{
		
		private String title;
		private Integer plantId;
		private Double price;
		private String description;
		
		@Override
	    public String toString(){
	      	return title + " " + description + " " + price;
	    }
}
