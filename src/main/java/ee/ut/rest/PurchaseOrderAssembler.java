package ee.ut.rest;

import ee.ut.controller.Servers;
import ee.ut.domain.Planthirerequest;

public class PurchaseOrderAssembler {
	public PurchaseOrderResource toResource(Planthirerequest req){
		PurchaseOrderResource res = new PurchaseOrderResource();
		res.setEnd(req.getEndDate().getTime());
		res.setHireRequestId(req.getId());
		PlantResource plant = new PlantResource();
		plant.setPlantId(req.getPlant());
		res.setPlant(plant);
		res.setSiteId(req.getSiteId());
		res.setStart(req.getStartDate().getTime());
		res.setPoId(req.getPoid());
		//res.setStatus(req.getStatus());
		res.setEmail(Servers.email);
		res.setCredentials(Servers.credentials);
		res.setServer(Servers.buildit);
		res.setPrice(req.getPrice());
		return res;
	}
}
