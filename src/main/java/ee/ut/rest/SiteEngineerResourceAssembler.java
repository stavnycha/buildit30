package ee.ut.rest;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import ee.ut.controller.RESTController;
import ee.ut.domain.Siteengineer;

public class SiteEngineerResourceAssembler 
extends ResourceAssemblerSupport<Siteengineer, SiteEngineerResource>{
	
	public  SiteEngineerResourceAssembler () { 
		 super(RESTController.class, SiteEngineerResource.class); 
	}

	@Override
	public SiteEngineerResource toResource(Siteengineer entity) {
		SiteEngineerResource se = new SiteEngineerResource();
		se.setEmail(entity.getEmail());
		se.setName(entity.getName());
		se.setEngineerId(entity.getId());
		return se;
	}
}
