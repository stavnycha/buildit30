package ee.ut.rest;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import ee.ut.controller.RESTController;
import ee.ut.domain.Workengineer;

public class WorkEngineerResourceAssembler 
extends ResourceAssemblerSupport<Workengineer, WorkEngineerResource>{
	
	public WorkEngineerResourceAssembler () { 
		 super(RESTController.class, WorkEngineerResource.class); 
	}

	@Override
	public WorkEngineerResource toResource(Workengineer entity) {
		WorkEngineerResource we = new WorkEngineerResource();
		we.setEmail(entity.getEmail());
		we.setName(entity.getName());
		we.setWorkId(entity.getId());
		return we;
	}
}
