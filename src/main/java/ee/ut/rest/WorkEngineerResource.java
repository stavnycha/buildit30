package ee.ut.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.roo.addon.javabean.RooJavaBean;
import ee.ut.utils.ResourceSupport;

@RooJavaBean
@XmlRootElement(name = "workengineer")
@XmlAccessorType(XmlAccessType.FIELD)
public class WorkEngineerResource extends ResourceSupport {
	private String name;
	private String email;
	private Long workId;

	public String getName() {
        return this.name;
    }

	public void setName(String name) {
        this.name = name;
    }

	public String getEmail() {
        return this.email;
    }

	public void setEmail(String email) {
        this.email = email;
    }

	public Long getWorkId() {
        return this.workId;
    }

	public void setWorkId(Long workId) {
        this.workId = workId;
    }
}
