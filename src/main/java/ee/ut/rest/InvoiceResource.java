package ee.ut.rest;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.roo.addon.javabean.RooJavaBean;

import ee.ut.domain.InvoiceStatus;
import ee.ut.utils.ResourceSupport;

@RooJavaBean
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class InvoiceResource extends ResourceSupport{

	private Long invoiceId;
    private Date date;
	private InvoiceStatus status;
	private Double total;
	private Long requestId;
	private Long poId;

}