package ee.ut.rest.rentit1;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import ee.ut.controller.rentits.RentitServers;
import ee.ut.domain.Planthirerequest;

public class InvoiceResourceConverter {

	public ee.ut.rest.InvoiceResource toResource(InvoiceResource i) throws ParseException{
		ee.ut.rest.InvoiceResource res = new ee.ut.rest.InvoiceResource();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		res.setDate(format.parse(i.getDate()));
		res.setInvoiceId(i.getInvoiceId());
		res.setPoId(i.getPurchaseOrderId());
		res.setStatus(ee.ut.domain.InvoiceStatus.NOT_PAID);
		res.setTotal(i.getTotal().doubleValue());
		Planthirerequest phr = Planthirerequest.findPlanthirerequestByPOId(i.getPurchaseOrderId(), RentitServers.RENTIT_1);
		res.setRequestId(phr.getId());
		return res;
	}
}
