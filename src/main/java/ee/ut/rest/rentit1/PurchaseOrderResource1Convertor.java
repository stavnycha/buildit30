package ee.ut.rest.rentit1;

import java.math.BigDecimal;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import ee.ut.controller.RESTController;
import ee.ut.controller.Servers;

public class PurchaseOrderResource1Convertor extends
	ResourceAssemblerSupport<ee.ut.rest.PurchaseOrderResource, PurchaseOrderResource> {

	public PurchaseOrderResource1Convertor() {
		super(RESTController.class, PurchaseOrderResource.class);
	}
	
	@Override
	public PurchaseOrderResource toResource(
			ee.ut.rest.PurchaseOrderResource p) {
		PurchaseOrderResource res = null;
		if (p.getPoId() != null)
			res = createResourceWithId(p.getPoId(), p);
		else 
			res = new PurchaseOrderResource();
		PlantResourceConverter conv = new PlantResourceConverter();
		
		res.setEnddate(p.getEnd());
		res.setStartdate(p.getStart());
		BigDecimal price;
		if (p.getPrice() != null)
			price = new BigDecimal(p.getPrice());
		else
			price = null;
	
		res.setPrice(price);
		res.setPoExtensionRejectionlink(Servers.buildit + "/rest/requests/rejectPHRExtension");
		res.setPoRejectionlink(Servers.buildit + "/rest/requests/rejectPHR");
		res.setCustomerEmail(Servers.email);
		res.setPlantId(conv.fromResource(p.getPlant()));
		return res;
	
	}
}
