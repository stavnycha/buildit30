package ee.ut.rest.rentit1;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;
import org.springframework.roo.addon.javabean.RooJavaBean;

@RooJavaBean
@XmlRootElement(name="plant")
@JsonRootName(value = "plant")
public class PlantResource {
	
	private Long id;
	private String name;
    private BigDecimal pricePrDay;
    
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getPricePrDay() {
		return pricePrDay;
	}
	public void setPricePrDay(BigDecimal pricePrDay) {
		this.pricePrDay = pricePrDay;
	}
}


