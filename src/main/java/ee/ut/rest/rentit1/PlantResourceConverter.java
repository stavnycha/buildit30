package ee.ut.rest.rentit1;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class PlantResourceConverter {

	public ee.ut.rest.PlantResource toResource (PlantResource p){
		ee.ut.rest.PlantResource res = new ee.ut.rest.PlantResource();
		res.setDescription("");
		res.setPlantId(p.getId().intValue());
		res.setPrice(p.getPricePrDay().doubleValue());
		res.setTitle(p.getName());
		return res;
	}
	
	public PlantResource fromResource (ee.ut.rest.PlantResource p){
		PlantResource res = new PlantResource();
		res.setId(p.getPlantId().longValue());
		res.setName(p.getTitle());
		BigDecimal price;
		if (p.getPrice() != null)
			price = new BigDecimal(p.getPrice());
		else
			price = null;
		res.setPricePrDay(price);
		return res;
	}
	
	public ee.ut.rest.PlantResourceList toResource(PlantResourceList plist){
		ee.ut.rest.PlantResourceList res = new ee.ut.rest.PlantResourceList();
		List<ee.ut.rest.PlantResource> plants = new LinkedList<ee.ut.rest.PlantResource>();
		for (int i = 0; i < plist.getPlantResources().size(); i++) {
			plants.add(toResource(plist.getPlantResources().get(i)));
		}
		res.setPlants(plants);
		return res;
	}
}
