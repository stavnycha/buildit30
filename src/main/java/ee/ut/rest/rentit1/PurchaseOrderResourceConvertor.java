package ee.ut.rest.rentit1;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import ee.ut.controller.RESTController;



public class PurchaseOrderResourceConvertor extends
	ResourceAssemblerSupport<PurchaseOrderResource, ee.ut.rest.PurchaseOrderResource> {

	public PurchaseOrderResourceConvertor() {
		super(RESTController.class, ee.ut.rest.PurchaseOrderResource.class);
	}

	@Override
	public ee.ut.rest.PurchaseOrderResource toResource(
			PurchaseOrderResource p) {
		//ee.ut.rest.PurchaseOrderResource res = createResourceWithId(p.getId(), p);
		ee.ut.rest.PurchaseOrderResource res = new ee.ut.rest.PurchaseOrderResource();
		PlantResourceConverter conv = new PlantResourceConverter();
		res.setEnd(p.getEnddate());
		res.setStart(p.getStartdate());
		res.setPrice(p.getPrice().doubleValue());
		res.setPlant(conv.toResource(p.getPlantId()));
		String part = p.getId().toString().substring(p.getId().toString().lastIndexOf('/') + 1);
		part = part.substring(0, part.indexOf('>'));
		Long id = Long.valueOf(part);

		res.setPoId(id);
		res.setStatus(POstatus.getPOStatus(p.getStatus()));
		//todo: comment
		//phr id
		return res;
	}
	
}
