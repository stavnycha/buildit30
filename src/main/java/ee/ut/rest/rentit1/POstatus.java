package ee.ut.rest.rentit1;

public class POstatus {

	public final static short PENDING_CONFIRMATION = 0;
	public final static short REJECTED = -1;
	public final static short OPEN = 1;
	public final static short PENDING_UPDATE = 2;
	public final static short CLOSED = 3;
	public final static short Plant_Dispatched = 4;
	public final static short Plant_Delivered = 5;
	public final static short Plant_Rejected_By_Customer = 6;
	public final static short Plant_Returned = 7;
	public final static short Invoiced = 8;
	
	
public static String getStatusText(short status){
		
		switch(status){
			case -1:
				return "REJECTED";
			case 0:
				return "PENDING_CONFIRMATION";
			case 1:
				return "OPEN";
			case 2:
				return "PENDING_UPDATE";
			case 3:
				return "CLOSED";
			case 4:
				return "Plant_Dispatched";
			case 5:
				return "Plant_Delivered";
			case 6:
				return "Plant_Rejected_By_Customer";
			case 7:
				return "Plant_Returned";
			case 8:
				return "Invoiced";
		}
		
		return "";
	}
	
	public static ee.ut.rest.POStatus getPOStatus(short status){
		switch(status){
		case -1:
			return ee.ut.rest.POStatus.REJECTED;
		case 0:
			return ee.ut.rest.POStatus.PENDING_CONFIRMATION;
		case 1:
			return ee.ut.rest.POStatus.OPEN;
		case 3:
			return ee.ut.rest.POStatus.CLOSED;
		case 4:
			return ee.ut.rest.POStatus.PLANT_DISPATCHED;
		case 5:
			return ee.ut.rest.POStatus.PLANT_DELIVERED;
		case 6:
			return ee.ut.rest.POStatus.PLANT_REJECTED;
		case 7:
			return ee.ut.rest.POStatus.PLANT_RETURNED;
		case 8:
			return ee.ut.rest.POStatus.INVOICED;
		default:
			return ee.ut.rest.POStatus.OPEN;
	}
	
	}
	
	public static short getStatusID(String status){
		
		switch(status){
			case "REJECTED":
				return -1;
			case "PENDING_CONFIRMATION":
				return 0;
			case "OPEN":
				return 1;
			case "PENDING_UPDATE":
				return 2;
			case "CLOSED":
				return 3;
			case "Plant_Dispatched":
				return 4;
			case "Plant_Delivered":
				return 5;
			case "Plant_Rejected_By_Customer":
				return 6;
			case "Plant_Returned":
				return 7;
			case "Invoiced":
				return 8;
		}
		
		return 0;
	}
}

