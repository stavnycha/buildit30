package ee.ut.rest.rentit1;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.roo.addon.javabean.RooJavaBean;

import com.fasterxml.jackson.annotation.JsonRootName;

@RooJavaBean
@XmlRootElement(name = "Invoice")
@JsonRootName(value = "Invoice")
public class InvoiceResource{
	
	private long invoiceId;
	private long purchaseOrderId;
	private BigDecimal total;
	private String date;
	private String rentitEmailAddress;
	
	
	public long getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(long invoiceId) {
		this.invoiceId = invoiceId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public long getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(long purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public String getRentitEmailAddress() {
		return rentitEmailAddress;
	}
	public void setRentitEmailAddress(String rentitEmailAddress) {
		this.rentitEmailAddress = rentitEmailAddress;
	}
	
}

