package ee.ut.rest.rentit1;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;
import org.springframework.roo.addon.javabean.RooJavaBean;

@RooJavaBean
@XmlRootElement(name="plants")
@JsonRootName(value = "plants")
public class PlantResourceList {
	private List<PlantResource> plantResources = new ArrayList<PlantResource>();
	
	public PlantResourceList() {
		
	}
	
	public PlantResourceList(List<PlantResource> plantResources){
		this.plantResources = plantResources;
	}
	
	public List<PlantResource> getPlantResources() {
		return plantResources;
	}

	public void setPlantResources(List<PlantResource> plantResources) {
		this.plantResources = plantResources;
	}
}


