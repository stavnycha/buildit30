package ee.ut.rest.rentit1;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;
import org.springframework.roo.addon.javabean.RooJavaBean;

@RooJavaBean
@XmlRootElement(name = "POS")
@JsonRootName(value = "POS")
public class PurchaseOrderResourceList{
private List<PurchaseOrderResource> po = new ArrayList<PurchaseOrderResource>();
	
	public PurchaseOrderResourceList() {
		
	}
	
	public PurchaseOrderResourceList(List<PurchaseOrderResource> PurchaseOrderResources){
		this.po = PurchaseOrderResources;
	}

	public List<PurchaseOrderResource> getPo() {
		return po;
	}

	public void setPo(List<PurchaseOrderResource> po) {
		this.po = po;
	}
}

