package ee.ut.rest;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import ee.ut.utils.DateAdapter;
import ee.ut.utils.ResourceSupport;

@XmlRootElement(name = "purchaseorder")
@XmlAccessorType(XmlAccessType.FIELD)
public class PurchaseOrderResource extends ResourceSupport  {
	
	private Long hireRequestId;
	private PlantResource plant;
	private Long poId;
	private Double price;
	private Integer siteId;
	private POStatus status;
	private String email;
    private String server;
    private String credentials;
    
    public String getCredentials() {
		return credentials;
	}

	public void setCredentials(String credentials) {
		this.credentials = credentials;
	}
    
    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String s) {
		this.server = s;
	}

	private Date startDate;
	
	private Date endDate;

	public Long getHireRequestId() {
        return this.hireRequestId;
    }

	public void setHireRequestId(Long hireRequestId) {
        this.hireRequestId = hireRequestId;
    }

	public PlantResource getPlant() {
        return this.plant;
    }

	public void setPlant(PlantResource plant) {
        this.plant = plant;
    }

	public Long getPoId() {
        return this.poId;
    }

	public void setPoId(Long poId) {
        this.poId = poId;
    }

	public Double getPrice() {
        return this.price;
    }

	public void setPrice(Double price) {
        this.price = price;
    }

	public Integer getSiteId() {
        return this.siteId;
    }

	public void setSiteId(Integer siteId) {
        this.siteId = siteId;
    }

	public POStatus getStatus() {
        return this.status;
    }

	public void setStatus(POStatus status) {
        this.status = status;
    }

	@XmlJavaTypeAdapter(DateAdapter.class)
	@XmlElement(name="start")
	public Date getStart() {
        return this.startDate;
    }

	public void setStart(Date start) {
        this.startDate = start;
    }

	@XmlJavaTypeAdapter(DateAdapter.class)
	@XmlElement(name="end")
	public Date getEnd() {
        return this.endDate;
    }

	public void setEnd(Date end) {
        this.endDate = end;
    }
}
