package ee.ut.rest;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;

@RooJavaBean
public class QueriedPlant {
	private String name;
	
	@Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
	private Calendar startDate;
	
	@Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
	private Calendar endDate;
	
}
