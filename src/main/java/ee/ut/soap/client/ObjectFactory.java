
package ee.ut.soap.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ee.ut.soap.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PlantResource_QNAME = new QName("http://web.soap.ut.ee/", "plantResource");
    private final static QName _Update_QNAME = new QName("http://web.soap.ut.ee/", "update");
    private final static QName _PayInvoice_QNAME = new QName("http://web.soap.ut.ee/", "payInvoice");
    private final static QName _PayInvoiceResponse_QNAME = new QName("http://web.soap.ut.ee/", "payInvoiceResponse");
    private final static QName _PurchaseOrderResource_QNAME = new QName("http://web.soap.ut.ee/", "purchaseOrderResource");
    private final static QName _InvalidHirePeriodException_QNAME = new QName("http://web.soap.ut.ee/", "InvalidHirePeriodException");
    private final static QName _CreatePO_QNAME = new QName("http://web.soap.ut.ee/", "createPO");
    private final static QName _PlantUnavailableException_QNAME = new QName("http://web.soap.ut.ee/", "PlantUnavailableException");
    private final static QName _CreatePOResponse_QNAME = new QName("http://web.soap.ut.ee/", "createPOResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ee.ut.soap.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PoUpdateResource }
     * 
     */
    public PoUpdateResource createPoUpdateResource() {
        return new PoUpdateResource();
    }

    /**
     * Create an instance of {@link PlantResource }
     * 
     */
    public PlantResource createPlantResource() {
        return new PlantResource();
    }

    /**
     * Create an instance of {@link PayInvoice }
     * 
     */
    public PayInvoice createPayInvoice() {
        return new PayInvoice();
    }

    /**
     * Create an instance of {@link PurchaseOrderResource }
     * 
     */
    public PurchaseOrderResource createPurchaseOrderResource() {
        return new PurchaseOrderResource();
    }

    /**
     * Create an instance of {@link PayInvoiceResponse }
     * 
     */
    public PayInvoiceResponse createPayInvoiceResponse() {
        return new PayInvoiceResponse();
    }

    /**
     * Create an instance of {@link CreatePOResponse }
     * 
     */
    public CreatePOResponse createCreatePOResponse() {
        return new CreatePOResponse();
    }

    /**
     * Create an instance of {@link PlantUnavailableException }
     * 
     */
    public PlantUnavailableException createPlantUnavailableException() {
        return new PlantUnavailableException();
    }

    /**
     * Create an instance of {@link CreatePO }
     * 
     */
    public CreatePO createCreatePO() {
        return new CreatePO();
    }

    /**
     * Create an instance of {@link InvalidHirePeriodException }
     * 
     */
    public InvalidHirePeriodException createInvalidHirePeriodException() {
        return new InvalidHirePeriodException();
    }

    /**
     * Create an instance of {@link Plant }
     * 
     */
    public Plant createPlant() {
        return new Plant();
    }

    /**
     * Create an instance of {@link Invoice }
     * 
     */
    public Invoice createInvoice() {
        return new Invoice();
    }

    /**
     * Create an instance of {@link PoUpdate }
     * 
     */
    public PoUpdate createPoUpdate() {
        return new PoUpdate();
    }

    /**
     * Create an instance of {@link PurchaseOrder }
     * 
     */
    public PurchaseOrder createPurchaseOrder() {
        return new PurchaseOrder();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlantResource }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web.soap.ut.ee/", name = "plantResource")
    public JAXBElement<PlantResource> createPlantResource(PlantResource value) {
        return new JAXBElement<PlantResource>(_PlantResource_QNAME, PlantResource.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PoUpdateResource }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web.soap.ut.ee/", name = "update")
    public JAXBElement<PoUpdateResource> createUpdate(PoUpdateResource value) {
        return new JAXBElement<PoUpdateResource>(_Update_QNAME, PoUpdateResource.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayInvoice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web.soap.ut.ee/", name = "payInvoice")
    public JAXBElement<PayInvoice> createPayInvoice(PayInvoice value) {
        return new JAXBElement<PayInvoice>(_PayInvoice_QNAME, PayInvoice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PayInvoiceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web.soap.ut.ee/", name = "payInvoiceResponse")
    public JAXBElement<PayInvoiceResponse> createPayInvoiceResponse(PayInvoiceResponse value) {
        return new JAXBElement<PayInvoiceResponse>(_PayInvoiceResponse_QNAME, PayInvoiceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurchaseOrderResource }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web.soap.ut.ee/", name = "purchaseOrderResource")
    public JAXBElement<PurchaseOrderResource> createPurchaseOrderResource(PurchaseOrderResource value) {
        return new JAXBElement<PurchaseOrderResource>(_PurchaseOrderResource_QNAME, PurchaseOrderResource.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvalidHirePeriodException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web.soap.ut.ee/", name = "InvalidHirePeriodException")
    public JAXBElement<InvalidHirePeriodException> createInvalidHirePeriodException(InvalidHirePeriodException value) {
        return new JAXBElement<InvalidHirePeriodException>(_InvalidHirePeriodException_QNAME, InvalidHirePeriodException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatePO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web.soap.ut.ee/", name = "createPO")
    public JAXBElement<CreatePO> createCreatePO(CreatePO value) {
        return new JAXBElement<CreatePO>(_CreatePO_QNAME, CreatePO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlantUnavailableException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web.soap.ut.ee/", name = "PlantUnavailableException")
    public JAXBElement<PlantUnavailableException> createPlantUnavailableException(PlantUnavailableException value) {
        return new JAXBElement<PlantUnavailableException>(_PlantUnavailableException_QNAME, PlantUnavailableException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatePOResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://web.soap.ut.ee/", name = "createPOResponse")
    public JAXBElement<CreatePOResponse> createCreatePOResponse(CreatePOResponse value) {
        return new JAXBElement<CreatePOResponse>(_CreatePOResponse_QNAME, CreatePOResponse.class, null, value);
    }

}
