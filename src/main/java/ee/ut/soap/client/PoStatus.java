
package ee.ut.soap.client;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for poStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="poStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PENDING_CONFIRMATION"/>
 *     &lt;enumeration value="OPEN"/>
 *     &lt;enumeration value="PENDING_UPDATE"/>
 *     &lt;enumeration value="CLOSED"/>
 *     &lt;enumeration value="REJECTED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "poStatus")
@XmlEnum
public enum PoStatus {

    PENDING_CONFIRMATION,
    OPEN,
    PENDING_UPDATE,
    CLOSED,
    REJECTED;

    public String value() {
        return name();
    }

    public static PoStatus fromValue(String v) {
        return valueOf(v);
    }

}
