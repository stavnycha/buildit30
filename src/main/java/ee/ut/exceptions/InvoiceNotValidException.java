package ee.ut.exceptions;

public class InvoiceNotValidException  extends Exception {
	public InvoiceNotValidException  (String message){super(message);}
}
