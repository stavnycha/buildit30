package ee.ut.exceptions;

@SuppressWarnings("serial") 
public class MethodNotAllowedException extends Exception {
	public MethodNotAllowedException  (String message){super(message);}
}
