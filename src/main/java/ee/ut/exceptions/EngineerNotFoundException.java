package ee.ut.exceptions;

@SuppressWarnings("serial") 
public class EngineerNotFoundException extends Exception{
	public EngineerNotFoundException (String message){super(message);} 
}
