package ee.ut.exceptions;

@SuppressWarnings("serial") 
public class InvalidHirePeriodException extends Exception { 
	public InvalidHirePeriodException(String message){super(message);} 
}