package ee.ut.exceptions;

@SuppressWarnings("serial") 
public class RequestNotFoundException extends Exception{
	public RequestNotFoundException (String message){super(message);} 
}
