package ee.ut.security;

import javax.persistence.Column;
import javax.persistence.ManyToOne;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(versionField = "", table = "assignment", schema = "public")
public class Assignment {
	
	/**
     */
    @ManyToOne
	private Users userbuildit;
	
    /**
     */
    @ManyToOne
	private Authorities authority;
}
