package ee.ut.web;
import ee.ut.domain.Workengineer;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/workengineers")
@Controller
@RooWebScaffold(path = "workengineers", formBackingObject = Workengineer.class)
public class WorkengineerController {
}
