package ee.ut.web.we;
import ee.ut.domain.*;

import org.joda.time.format.DateTimeFormat;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import beans.POModification;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import ee.ut.controller.RentitAPIController;
import ee.ut.controller.Servers;
import ee.ut.exceptions.MethodNotAllowedException;
import ee.ut.exceptions.RequestNotFoundException;
import ee.ut.rest.PlantHireRequestResource;
import ee.ut.rest.PurchaseOrderAssembler;
import ee.ut.rest.PurchaseOrderResource;
import ee.ut.rest.RejectBean;

import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Calendar;

@RequestMapping("/we/phrs")
@Controller
@RooWebScaffold(path = "we/phrs", formBackingObject = Planthirerequest.class, create = false, delete = false, update = false)
public class PHRWEController {
	
	@RequestMapping(value = "/{id}/accept", method = RequestMethod.GET)
	public String accept(@PathVariable("id") Long id) throws RequestNotFoundException, MethodNotAllowedException, JsonProcessingException{
		Planthirerequest req = Planthirerequest.findPlanthirerequest(id);
		if (req == null)
			throw new RequestNotFoundException("Plant Hire Request not found.");
		if (req.getStatus() != RequestStatus.REJECTED && req.getStatus() != RequestStatus.PENDING_CONFIRMATION)
			throw new MethodNotAllowedException("Method is not allowed.");
		
		PurchaseOrderAssembler assembler = new PurchaseOrderAssembler();
		PurchaseOrderResource resource = assembler.toResource(req);
		PurchaseOrderResource res = null;
		if (req.getPoid() == null)
			res = RentitAPIController.createPO(resource);
		else {
			POModification m = new POModification();
			m.setEndDate(req.getEndDate().getTime());
			m.setStartDate(req.getStartDate().getTime());
			m.setId(req.getId());
			res = RentitAPIController.modifyPO(m);
		}
			
		if (res != null){
			req.setStatus(RequestStatus.APPROVED);
			req.setPoid(res.getPoId());
		} else {
			req.setStatus(RequestStatus.REJECTED);
		}
		/*ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(resource);
		
		HttpEntity<String> requestEntity = new HttpEntity<String>(json, Servers.getHeaders("buildit:buildit"));
		RestTemplate template = new RestTemplate();
		//String res = template.postForObject(server + "/rest/po", requestEntity, String.class);
		//System.out.println(res);
		ResponseEntity<PurchaseOrderResource> res = template.exchange(server + "/rest/po", 
				  HttpMethod.POST, requestEntity, PurchaseOrderResource.class);*/

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String name = authentication.getName();
	    Workengineer we = Workengineer.findWorkengineersByEmailEquals(name).getSingleResult(); 
	    req.setWorkEngineer(we);
		
    	/*ResponseEntity<PurchaseOrderResource> createPO = 
    			template.exchange(res.getHeaders().getLocation(), 
    					HttpMethod.GET, requestEntity, PurchaseOrderResource.class);
    	PurchaseOrderResource po = createPO.getBody();*/
		
    	req.persist();
		/*Client client = Client.create();
	    
		WebResource webResource = client.resource(server + "/rest/po");
	    ClientResponse response = webResource.type(MediaType.APPLICATION_XML) 
	       	 .accept(MediaType.APPLICATION_XML).post(ClientResponse.class, resource);

	    System.out.println(response.getLocation());
	    webResource = client.resource(response.getLocation());

	    resource = webResource.type(MediaType.APPLICATION_XML) 
		       	 .accept(MediaType.APPLICATION_XML).get(PurchaseOrderResource.class); */
		
		return "redirect:/we/phrs?page=1&size=50";
	}
	
	@RequestMapping(value = "/{id}/reject", method = RequestMethod.POST, produces = "text/html")
	public String reject(@Valid RejectBean bean) throws RequestNotFoundException, MethodNotAllowedException {
		Planthirerequest req = Planthirerequest.findPlanthirerequest(bean.getId());
		if (req == null)
			throw new RequestNotFoundException("Plant Hire Request not found.");
		if (req.getStatus() != RequestStatus.PENDING_CONFIRMATION)
			throw new MethodNotAllowedException("Method is not allowed.");
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String name = authentication.getName();
	    Workengineer we = Workengineer.findWorkengineersByEmailEquals(name).getSingleResult(); 
	    req.setWorkEngineer(we);
	    
		req.setStatus(RequestStatus.REJECTED);
		req.setComment(bean.getComment());
		req.persist();
		return "redirect:/we/phrs?page=1&size=50";
	}

    String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }

    void populateEditForm(Model uiModel, Planthirerequest planthirerequest) {
        uiModel.addAttribute("planthirerequest", planthirerequest);
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("invoices", Invoice.findAllInvoices());
        uiModel.addAttribute("requeststatuses", Arrays.asList(RequestStatus.values()));
        uiModel.addAttribute("requestupdates", RequestUpdate.findAllRequestUpdates());
        uiModel.addAttribute("siteengineers", Siteengineer.findAllSiteengineers());
        uiModel.addAttribute("workengineers", Workengineer.findAllWorkengineers());
    }

    void addDateTimeFormatPatterns(Model uiModel) {
        uiModel.addAttribute("planthirerequest_startdate_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
        uiModel.addAttribute("planthirerequest_enddate_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
    }

    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        
        populateEditForm(uiModel, Planthirerequest.findPlanthirerequest(id));
        return "we/phrs/update";
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid POModification modification, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        /*if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, planthirerequest);
            return "we/phrs/update";
        }*/
        uiModel.asMap().clear();
        Planthirerequest phr = Planthirerequest.findPlanthirerequest(modification.getId());
        
       
    	
        if (phr.getStatus() != RequestStatus.PENDING_CONFIRMATION){
        	Calendar c = Calendar.getInstance();
         	c.setTime(modification.getStartDate());
         	phr.setStartDate(c);
         	c.setTime(modification.getEndDate());
         	phr.setEndDate(c);
        } else {
        	//Ren
        }
        
        //RentitAPIController
        //planthirerequest.merge();
        return "redirect:/we/phrs/" + encodeUrlPathSegment(modification.getId().toString(), httpServletRequest);
    }

    @RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("planthirerequests", Planthirerequest.findPlanthirerequestEntries(firstResult, sizeNo));
            float nrOfPages = (float) Planthirerequest.countPlanthirerequests() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("planthirerequests", Planthirerequest.findAllPlanthirerequests());
        }
        uiModel.addAttribute("rejectBean", new RejectBean());
        addDateTimeFormatPatterns(uiModel);
        return "we/phrs/list";
    }

    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
    	
    	Planthirerequest phr = Planthirerequest.findPlanthirerequest(id);

        populateEditForm(uiModel, phr);

        try {
        	if(phr.getPoid() != null){
            	uiModel.addAttribute("poStatus", RentitAPIController.getPO(phr).getStatus().name());
            }
        } catch (Exception e){
        	e.printStackTrace();
        }
        uiModel.addAttribute("itemId", id);
        return "we/phrs/show";
    }
}
