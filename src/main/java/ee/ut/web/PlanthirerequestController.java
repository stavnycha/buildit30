package ee.ut.web;
import ee.ut.controller.RentitAPIController;
import ee.ut.controller.Servers;
import ee.ut.domain.Invoice;
import ee.ut.domain.InvoiceStatus;
import ee.ut.domain.Planthirerequest;
import ee.ut.domain.RequestStatus;
import ee.ut.domain.RequestUpdate;
import ee.ut.domain.Siteengineer;
import ee.ut.domain.Workengineer;
import ee.ut.exceptions.MethodNotAllowedException;
import ee.ut.exceptions.RequestNotFoundException;
import ee.ut.rest.PurchaseOrderAssembler;
import ee.ut.rest.PurchaseOrderResource;
import ee.ut.soap.client.PurchaseOrderSOAPService;
import ee.ut.soap.client.PurchaseOrderSOAPServiceService;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpEntity;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@RequestMapping("/planthirerequests")
@Controller
@RooWebScaffold(path = "planthirerequests", formBackingObject = Planthirerequest.class)
public class PlanthirerequestController {

	private PurchaseOrderSOAPService service = new PurchaseOrderSOAPServiceService().getPurchaseOrderSOAPServicePort();
	
	
	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Planthirerequest planthirerequest, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, planthirerequest);
            return "planthirerequests/create";
        }
        uiModel.asMap().clear();
        if (planthirerequest.getStatus() == null)
        	planthirerequest.setStatus(RequestStatus.PENDING_CONFIRMATION);
        	
        planthirerequest.persist();
        return "redirect:/planthirerequests/" + encodeUrlPathSegment(planthirerequest.getId().toString(), httpServletRequest);
    }

	@RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        populateEditForm(uiModel, new Planthirerequest());
        return "planthirerequests/create";
    }

	@RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        addDateTimeFormatPatterns(uiModel);

        Planthirerequest phr = Planthirerequest.findPlanthirerequest(id);

        try {
        	if(phr.getPoid() != null){
            	uiModel.addAttribute("poStatus", RentitAPIController.getPO(phr).getStatus().name());
            }
        } catch (Exception e){
        	e.printStackTrace();
        }

        uiModel.addAttribute("planthirerequest",phr );
        uiModel.addAttribute("itemId", id);
        return "planthirerequests/show";
    }

	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("planthirerequests", Planthirerequest.findPlanthirerequestEntries(firstResult, sizeNo));
            float nrOfPages = (float) Planthirerequest.countPlanthirerequests() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("planthirerequests", Planthirerequest.findAllPlanthirerequests());
        }
        addDateTimeFormatPatterns(uiModel);
        return "planthirerequests/list";
    }

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Planthirerequest planthirerequest, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, planthirerequest);
            return "planthirerequests/update";
        }
        uiModel.asMap().clear();
        planthirerequest.merge();
        return "redirect:/planthirerequests/" + encodeUrlPathSegment(planthirerequest.getId().toString(), httpServletRequest);
    }

	@RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, Planthirerequest.findPlanthirerequest(id));
        return "planthirerequests/update";
    }

	/*@RequestMapping(value = "/accept/{id}", method = RequestMethod.GET)
	public String accept(@PathVariable("id") Long id) throws RequestNotFoundException, MethodNotAllowedException{
		Planthirerequest req = Planthirerequest.findPlanthirerequest(id);
		if (req == null)
			throw new RequestNotFoundException("Plant Hire Request not found.");
		if (req.getStatus() != RequestStatus.REJECTED && req.getStatus() != RequestStatus.PENDING_CONFIRMATION)
			throw new MethodNotAllowedException("Method is not allowed.");
		req.setStatus(RequestStatus.APPROVED);
		req.persist();
		
		PurchaseOrderAssembler assembler = new PurchaseOrderAssembler();
		PurchaseOrderResource resource = assembler.toResource(req);
		
		Client client = Client.create();
	    
		WebResource webResource = client.resource(server + "/rest/po");
	    ClientResponse response = webResource.type(MediaType.APPLICATION_XML) 
	       	 .accept(MediaType.APPLICATION_XML).post(ClientResponse.class, resource);

	    System.out.println(response.getLocation());
	    webResource = client.resource(response.getLocation());

	    resource = webResource.type(MediaType.APPLICATION_XML) 
		       	 .accept(MediaType.APPLICATION_XML).get(PurchaseOrderResource.class); 
		
		return "redirect:/planthirerequests/";
	}
	
	@RequestMapping(value = "/reject/{id}", method = RequestMethod.GET)
	public String reject(@PathVariable("id") Long id) throws RequestNotFoundException, MethodNotAllowedException {
		Planthirerequest req = Planthirerequest.findPlanthirerequest(id);
		if (req == null)
			throw new RequestNotFoundException("Plant Hire Request not found.");
		if (req.getStatus() != RequestStatus.PENDING_CONFIRMATION)
			throw new MethodNotAllowedException("Method is not allowed.");
		req.setStatus(RequestStatus.REJECTED);
		req.persist();
		return "redirect:/planthirerequests/";
	}*/
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        Planthirerequest planthirerequest = Planthirerequest.findPlanthirerequest(id);
        planthirerequest.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/planthirerequests";
    }
	
	/*@RequestMapping(method = RequestMethod.GET, value = "/acceptInvoice/{id}")
    public String acceptInvoice(@PathVariable Long id) {
		Invoice invoice = Invoice.findInvoice(id);
    	service.payInvoice(invoice.getInvoiceNumber());
    	invoice.setStatus(InvoiceStatus.PAID);
    	invoice.persist();
		return "redirect:/planthirerequests";
    }*/
	
	void addDateTimeFormatPatterns(Model uiModel) {
        uiModel.addAttribute("planthirerequest_startdate_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
        uiModel.addAttribute("planthirerequest_enddate_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
    }

	void populateEditForm(Model uiModel, Planthirerequest planthirerequest) {
        uiModel.addAttribute("planthirerequest", planthirerequest);
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("requeststatuses", Arrays.asList(RequestStatus.values()));
        uiModel.addAttribute("requestupdates", RequestUpdate.findAllRequestUpdates());
        uiModel.addAttribute("siteengineers", Siteengineer.findAllSiteengineers());
        uiModel.addAttribute("workengineers", Workengineer.findAllWorkengineers());
    }

	String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
}
