// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package ee.ut.web;

import ee.ut.security.Assignment;
import ee.ut.security.Authorities;
import ee.ut.security.Users;
import ee.ut.web.AssignmentsController;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

privileged aspect AssignmentsController_Roo_Controller {
    
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String AssignmentsController.create(@Valid Assignment assignment, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, assignment);
            return "security/assignments/create";
        }
        uiModel.asMap().clear();
        assignment.persist();
        return "redirect:/security/assignments/" + encodeUrlPathSegment(assignment.getId().toString(), httpServletRequest);
    }
    
    @RequestMapping(params = "form", produces = "text/html")
    public String AssignmentsController.createForm(Model uiModel) {
        populateEditForm(uiModel, new Assignment());
        return "security/assignments/create";
    }
    
    @RequestMapping(value = "/{id}", produces = "text/html")
    public String AssignmentsController.show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("assignment", Assignment.findAssignment(id));
        uiModel.addAttribute("itemId", id);
        return "security/assignments/show";
    }
    
    @RequestMapping(produces = "text/html")
    public String AssignmentsController.list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("assignments", Assignment.findAssignmentEntries(firstResult, sizeNo));
            float nrOfPages = (float) Assignment.countAssignments() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("assignments", Assignment.findAllAssignments());
        }
        return "security/assignments/list";
    }
    
    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String AssignmentsController.update(@Valid Assignment assignment, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, assignment);
            return "security/assignments/update";
        }
        uiModel.asMap().clear();
        assignment.merge();
        return "redirect:/security/assignments/" + encodeUrlPathSegment(assignment.getId().toString(), httpServletRequest);
    }
    
    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String AssignmentsController.updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, Assignment.findAssignment(id));
        return "security/assignments/update";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String AssignmentsController.delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        Assignment assignment = Assignment.findAssignment(id);
        assignment.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/security/assignments";
    }
    
    void AssignmentsController.populateEditForm(Model uiModel, Assignment assignment) {
        uiModel.addAttribute("assignment", assignment);
        uiModel.addAttribute("authoritieses", Authorities.findAllAuthoritieses());
        uiModel.addAttribute("userses", Users.findAllUserses());
    }
    
    String AssignmentsController.encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
    
}
