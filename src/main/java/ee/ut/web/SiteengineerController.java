package ee.ut.web;
import ee.ut.domain.Siteengineer;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;

@RequestMapping("/siteengineers")
@Controller
@RooWebScaffold(path = "siteengineers", formBackingObject = Siteengineer.class)
public class SiteengineerController {
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Siteengineer siteengineer, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, siteengineer);
            return "siteengineers/create";
        }
        uiModel.asMap().clear();
        siteengineer.persist();
        return "redirect:/siteengineers/" + encodeUrlPathSegment(siteengineer.getId().toString(), httpServletRequest);
    }

    void populateEditForm(Model uiModel, Siteengineer siteengineer) {
        uiModel.addAttribute("siteengineer", siteengineer);
    }

    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, Siteengineer.findSiteengineer(id));
        return "siteengineers/update";
    }

    @RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("siteengineer", Siteengineer.findSiteengineer(id));
        uiModel.addAttribute("itemId", id);
        return "siteengineers/show";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        Siteengineer siteengineer = Siteengineer.findSiteengineer(id);
        siteengineer.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/siteengineers";
    }

    @RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("siteengineers", Siteengineer.findSiteengineerEntries(firstResult, sizeNo));
            float nrOfPages = (float) Siteengineer.countSiteengineers() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("siteengineers", Siteengineer.findAllSiteengineers());
        }
        return "siteengineers/list";
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Siteengineer siteengineer, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, siteengineer);
            return "siteengineers/update";
        }
        uiModel.asMap().clear();
        siteengineer.merge();
        return "redirect:/siteengineers/" + encodeUrlPathSegment(siteengineer.getId().toString(), httpServletRequest);
    }

    String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }

    @RequestMapping(params = "form", produces = "text/html")
    public String createForm(Model uiModel) {
        populateEditForm(uiModel, new Siteengineer());
        return "siteengineers/create";
    }
}
