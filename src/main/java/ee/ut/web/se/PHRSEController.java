package ee.ut.web.se;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.xml.datatype.DatatypeConfigurationException;

import ee.ut.domain.*;
import ee.ut.rest.PlantResource;

import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import beans.ExtensionBean;
import beans.PHRBean;
import beans.PlantBean;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import ee.ut.controller.RentitAPIController;
import ee.ut.controller.Servers;
import ee.ut.controller.rentits.RentitServers;
import ee.ut.exceptions.EngineerNotFoundException;
import ee.ut.exceptions.InvalidHirePeriodException;
import ee.ut.rest.InvoiceResource;
import ee.ut.rest.PlantResourceList;
import ee.ut.rest.QueriedPlant;
import ee.ut.service.PlantHireRequestService;

import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequestMapping("/se/phrs")
@Controller
@RooWebScaffold(path = "se/phrs", formBackingObject = Planthirerequest.class, delete = false, create = false)
public class PHRSEController {

	@Autowired
	private PlantHireRequestService requestService;
	
	@RequestMapping(value = "/{id}", produces = "text/html")
    public String show(@PathVariable("id") Long id, Model uiModel) {
        addDateTimeFormatPatterns(uiModel);
        Planthirerequest phr = Planthirerequest.findPlanthirerequest(id);

        try {
        	if(phr.getPoid() != null){
            	uiModel.addAttribute("poStatus", RentitAPIController.getPO(phr).getStatus().name());
            }
        } catch (Exception e){
        	e.printStackTrace();
        }
        
        uiModel.addAttribute("planthirerequest",phr );
        uiModel.addAttribute("itemId", id);
        ExtensionBean bean = new ExtensionBean();
        bean.setId(id);
        uiModel.addAttribute("extension", bean);
        if (phr.getStatus() == RequestStatus.REJECTED)
        	uiModel.addAttribute("update", true);
        else 
        	uiModel.addAttribute("update", false);
        return "se/phrs/show";
    }
	
	 @RequestMapping(method = RequestMethod.GET, produces = "text/html", value = "/createPO")
	 public String searchPlant(Model modelMap) {
	  	addDateTimeFormatPatterns(modelMap);
	   	modelMap.asMap().put("plantquery", new QueriedPlant());
	   	return "se/phrs/search";
	 }  
	 
	 @RequestMapping(method = RequestMethod.POST, value = "/createPO") 
	 public String search(@Valid QueriedPlant bean, Model uiModel, HttpServletRequest request) throws DatatypeConfigurationException, JsonProcessingException { 
	   	QueriedPlant plant = (QueriedPlant) uiModel.asMap().get("queriedPlant");

         //call other groups RentitAPIController as above.
	   	List<PlantBean> finalList = new ArrayList<>();
	   	RentitServers servers = new RentitServers();
		for (String server: servers.getAllServers()){
			
			PlantResourceList plants = RentitAPIController.getAvailablePlants(plant, server);
			List<PlantBean> temp = new ArrayList<>();
			for (PlantResource p: plants.getPlants()){
				PlantBean pb = new PlantBean();
				pb.setDescription(p.getDescription());
				pb.setPlantId(p.getPlantId());
				pb.setPrice(p.getPrice());
				pb.setTitle(p.getTitle());
				pb.setServerPlant(server + "::" + p.getPlantId());
				temp.add(pb);
			}
			 //the final List to show
	         
	         //add other list make you do a check before adding
	         if(!temp.isEmpty()){
	             finalList.addAll(temp);
	         }
		}

        //if finallist is empty go back to search page.
        if(finalList.isEmpty()){
            String searchMsg = "No Plant Available";
            //addDateTimeFormatPatterns(uiModel);
            uiModel.asMap().put("plantquery", plant);
            request.setAttribute("searchMsg", searchMsg);
            return "se/phrs/search" ;
        }
			
		uiModel.addAttribute("plants", finalList);
		PHRBean req = new PHRBean();
		req.setEndDate(plant.getEndDate());
		req.setStartDate(plant.getStartDate());
		uiModel.addAttribute("hirerequest", req);
		return "se/phrs/create2";
    } 
	 
	@RequestMapping(method = RequestMethod.POST, value = "/submitRequest")
	public String submit(@Valid PHRBean bean, Model uiModel, HttpServletRequest request) throws DatatypeConfigurationException, InvalidHirePeriodException, EngineerNotFoundException { 
	    Planthirerequest req = new Planthirerequest();
	    req.setPlant(Integer.parseInt(bean.getPlant().split("::")[1]));
	    req.setServer(bean.getPlant().split("::")[0]);
	    req.setEndDate(bean.getEndDate());
	    req.setSiteId(bean.getSiteId());
	    req.setStartDate(bean.getStartDate());
	    req.setStatus(RequestStatus.PENDING_CONFIRMATION);
	    
		requestService.setPrice(req);

		
	    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	    String name = authentication.getName();
	    Siteengineer se = Siteengineer.findSiteengineersByEmailEquals(name).getSingleResult(); 
	    req.setSiteEngineer(se);

	    req.persist();
	    uiModel.addAttribute("planthirerequest", bean);

	    return "redirect:/se/phrs/" + encodeUrlPathSegment(req.getId().toString(), request);

	 } 
	
	@RequestMapping(produces = "text/html")
    public String list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	    String name = authentication.getName();
	    Siteengineer se = Siteengineer.findSiteengineersByEmailEquals(name).getSingleResult(); 
	    
		if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("planthirerequests", Planthirerequest.findPlanthirerequestEntriesBySiteEng(firstResult, sizeNo, se));
            float nrOfPages = (float) Planthirerequest.findPlanthirerequestEntriesBySiteEng(firstResult, sizeNo, se).size() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("planthirerequests", Planthirerequest.findPlanthirerequestsBySiteEngineer(se).getResultList());
        }
        addDateTimeFormatPatterns(uiModel);
        return "se/phrs/list";
    }

	
	@RequestMapping(method = RequestMethod.GET, value = "/acceptInvoice/{id}")
    public String acceptInvoice(@PathVariable Long id) {
		Invoice i = Invoice.findInvoice(id);
		Planthirerequest phr = Planthirerequest.findPlanthirerequestsByInvoice(i).getSingleResult();
		InvoiceResource res = new InvoiceResource();
		res.setDate(i.getDate());
		res.setInvoiceId(i.getInvoiceNumber());
		res.setPoId(phr.getPoid());
		res.setRequestId(phr.getId());
		res.setStatus(i.getStatus());
		res.setTotal(i.getTotal());
		requestService.payInvoice(res);
		
		return "redirect:/se/phrs?page=1&size=50";
    }
	
	@RequestMapping(method = RequestMethod.POST, value = "/extend")
    public String extendPO(@Valid ExtensionBean bean, Model uiModel, HttpServletRequest request) throws JsonProcessingException {
		requestService.extendPO(bean);

		return show(bean.getId(), uiModel);
    }
	
	@RequestMapping(method = RequestMethod.GET, value = "/cancel/{id}")
    public String cancelPO(@PathVariable Long id, Model uiModel) {
		boolean cancelled = requestService.cancelPO(id);
		if (cancelled){
			uiModel.addAttribute("cancelled", "PLANT HIRE REQUEST IS CANCELLED");
		} else {
			uiModel.addAttribute("cancelled", "CANCELLATION REJECTED");
		}
		return show(id, uiModel);
    }

    @RequestMapping(value = "/{id}", params = "form", method = RequestMethod.POST, produces = "text/html")
    public String update(@Valid PHRBean bean, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        Planthirerequest phr = Planthirerequest.findPlanthirerequest(bean.getId());
        phr.setStatus(RequestStatus.PENDING_CONFIRMATION);
        phr.setEndDate(bean.getEndDate());
        phr.setStartDate(bean.getStartDate());
	    phr.persist();
       
        return "redirect:/se/phrs/" + phr.getId();
    }

    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, Planthirerequest.findPlanthirerequest(id));
        PHRBean req = new PHRBean();
        Planthirerequest p = Planthirerequest.findPlanthirerequest(id);
        req.setEndDate(p.getEndDate());
        req.setSiteId(p.getSiteId());
        req.setStartDate(p.getStartDate());
        req.setId(p.getId());
        uiModel.addAttribute("hirerequest", req);
        return "se/phrs/update";
    }

    void populateEditForm(Model uiModel, Planthirerequest planthirerequest) {
        uiModel.addAttribute("planthirerequest", planthirerequest);
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("invoices", Invoice.findAllInvoices());
        uiModel.addAttribute("requeststatuses", Arrays.asList(RequestStatus.values()));
        uiModel.addAttribute("requestupdates", RequestUpdate.findAllRequestUpdates());
        uiModel.addAttribute("siteengineers", Siteengineer.findAllSiteengineers());
        uiModel.addAttribute("workengineers", Workengineer.findAllWorkengineers());

        //uiModel.addAttribute("plants", RentitAPIController.getAllPlants());
    }

    String encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }

    void addDateTimeFormatPatterns(Model uiModel) {
        uiModel.addAttribute("planthirerequest_startdate_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
        uiModel.addAttribute("planthirerequest_enddate_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
    }
}
